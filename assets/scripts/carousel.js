!(function(d){
  // Variables to target our base class,  get carousel items, count how many carousel items there are, set the slide to 0 (which is the number that tells us the frame we're on), and set motion to true which disables interactivity.
  
  var moving = true;
  // if (items.length === 0) { return; }
  // To initialise the carousel we'll want to update the DOM with our own classes
  function setInitialClasses(c) {

    // Target the last, initial, and next items and give them the relevant class.
    // This assumes there are three or more items.
    c.items[c.totalItems - 1].classList.add("prev");
    c.items[0].classList.add("active");
    c.items[1].classList.add("next");
  }

  function unify(e) { return e.changedTouches ? e.changedTouches[0] : e };

  function lock(e, c) {
    var unified = unify(e);
    c.x0 = unified.clientX;
    c.y0 = unified.clientY;
  };

  function move(e, c) {
    if(c.x0 || c.x0 === 0) {
      let unified = unify(e);
      let dx = unified.clientX - c.x0, s = Math.sign(dx);
      let dy = unified.clientY - c.y0;

      if (Math.abs(dx) > Math.abs(dy))
      {
        if (s > 0) { movePrev(c); }
        else if (s < 0) { moveNext(c); }
      }
    
      c.x0 = null;
      c.y0 = null;
    }
  };

  function removeStyle(el) {
    el.removeAttribute('style');
  }

  // Set click events to navigation buttons

  function setEventListeners(c) {
    var next = c.wrapper.getElementsByClassName('carousel__button--next')[0],
        prev = c.wrapper.getElementsByClassName('carousel__button--prev')[0];

    next.addEventListener('click', (e) => { moveNext(c) });
    prev.addEventListener('click', (e) => { movePrev(c) });

    var imgs = c.wrapper.getElementsByClassName('carousel__photo');

    var captions = c.wrapper.getElementsByTagName('figcaption');
    for (let caption of captions) {
      removeStyle(caption);
    }
    removeStyle(next);
    removeStyle(prev);

    for (let img of imgs) {
      img.addEventListener('mousedown', (e) => { lock(e, c) }, false);
      img.addEventListener('touchstart', (e) => { lock(e, c) }, false);

      img.addEventListener('mouseup', (e) => { move(e, c) }, false);
      img.addEventListener('touchend', (e) => { move(e, c) }, false);    
    }
  }

  // Disable interaction by setting 'moving' to true for the same duration as our transition (0.5s = 500ms)
  function disableInteraction(c) {
    moving = true;

    setTimeout(function(){
      moving = false
    }, 500);
  }

  function mod(n, m) {
    return ((n % m) + m) % m;
  }

  function moveCarouselTo(c, slide, dirNext) {

    // Check if carousel is moving, if not, allow interaction
    if(!moving) {

      // temporarily disable interactivity
      disableInteraction(c);

      // Preemptively set variables for the current next and previous slide, as well as the potential next or previous slide.
      var newPrevious = mod(slide - 1, c.totalItems),
          newNext = (slide + 1) % c.totalItems,
          oldPrevious = mod(slide - 2, c.totalItems),
          oldNext = (slide + 2) % c.totalItems;

      if (dirNext === 1) {
        c.items[slide].className = c.itemClassName + " next notransition";
        c.items[slide].offsetHeight;
        c.items[slide].classList.remove('notransition');
        c.items[newNext].className = c.itemClassName;
        c.items[newPrevious].className = c.itemClassName + " prev";
        c.items[slide].className = c.itemClassName + " active";
      }
      else {
        c.items[slide].className = c.itemClassName + " prev notransition";
        c.items[slide].offsetHeight;
        c.items[slide].classList.remove('notransition');
        c.items[newPrevious].className = c.itemClassName;
        c.items[newNext].className = c.itemClassName + " next";
        c.items[slide].className = c.itemClassName + " active";
      }
    }
  }

  // Next navigation handler
  function moveNext(c) {
    // Check if moving
    if (!moving) {

      // If it's the last slide, reset to 0, else +1
      if (c.slide === (c.totalItems - 1)) {
        c.slide = 0;
      } else {
        c.slide++;
      }

      // Move carousel to updated slide
      moveCarouselTo(c, c.slide, 1);
    }
  }

  // Previous navigation handler
  function movePrev(c) {

    // Check if moving
    if (!moving) {

      // If it's the first slide, set as the last slide, else -1
      if (c.slide === 0) {
        c.slide = (c.totalItems - 1);
      } else {
        c.slide--;
      }

      // Move carousel to updated slide
      moveCarouselTo(c, c.slide, 0);
    }
  }

  // Initialise carousel
  function initCarousel() {
    Array.from(d.getElementsByClassName("carousel")).forEach( (el) => {
      var carousel = {
        wrapper: el,
        itemClassName: "carousel__photo",
        slide: 0,
        x0: null,
        y0: null,
      }

      carousel.items = el.getElementsByClassName(carousel.itemClassName);
      if (carousel.items.length === 0) {
        var captions = carousel.wrapper.getElementsByTagName('figcaption');
        for (let caption of captions) {
          removeStyle(caption);
        }
        return;
      }
      carousel.itemClassName = carousel.items[0].className.replace('active', '');
      carousel.totalItems = carousel.items.length;


      if (carousel.items.length > 0) {
        setInitialClasses(carousel);
        setEventListeners(carousel);
      }

      // Set moving to false now that the carousel is ready
      moving = false;
      setInterval(() => {
        var div = d.querySelector('header .carousel');
        if (div && div.parentElement.querySelector(':hover') !== div && carousel.items.length > 1) {
          moveNext(carousel);
        }
      }, 4000);

    });

  }

  // make it rain
  initCarousel();


}(document));