---
---

let form = document.getElementById('comment-form');
form.addEventListener( "submit", function (e) {
    e.preventDefault();
    sendRequest();
  } );

function sendRequest() {
  var data = {};
    var elements = form.querySelectorAll( "input, select, textarea" );
    for( var i = 0; i < elements.length; ++i ) {
      var element = elements[i];
      var name = element.name;
      var value = element.value;
      data[ name ] = value;
    }

  var xhr = new XMLHttpRequest();
  xhr.open('POST', '{{ site.staticman.api }}', true);
  // xhr.open('POST', 'http://localhost:5555', true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange  = function () {
    disableForm();
    if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
      expandForm();
      addComment.cancelReply();
      form.reset();
      alert("{% t comments.alert %}");
      console.log("koniec");
    }
    else if (this.status !== 200) {
      alert("{% t comments.try_again %}");
    }
  };
  xhr.send(new URLSearchParams(data).toString());
  console.log("poczatek");
  disableForm();
}

function expandForm() {
  if (form.style.maxHeight === "0px" || form.style.maxHeight === "") {
    form.style.maxHeight = "800px";
  }
  else {
    form.style.maxHeight = "0px";
  }
}

function disableForm() {
  var elements = form.elements;
  var button = form.querySelector('#comment-form-submit');
  var buttonText = button.querySelector('span');
  var icon = button.querySelector('svg');
  if (elements[0].disabled) {
    for (var i = 0, len = elements.length; i < len; ++i) {
        elements[i].disabled = false;
    }
    buttonText.innerText = "{% t comments.submit %}";
    icon.style.display = "none";
    button.style = "";

  }
  else {
    for (var i = 0, len = elements.length; i < len; ++i) {
        elements[i].disabled = true;
    }
    buttonText.innerText = "\xa0{% t comments.processing %}...";
    icon.style.display = "inline";
    button.style.backgroundColor = "white";
    button.style.color = "#398981";
  }
}


var addComment = {
  currentButton: null,
  currentFun: null,
  cancelReply: function() {
    var t       = addComment,
      temp    = t.I( 'sm-temp-form-div' ),
      form        = t.I("comment-form"),
      formButton  = t.I("form-button"),
      cancel      = t.I( 'cancel-comment-reply-link' ),
      respond = t.I( t.respondId );

    if ( ! temp || ! respond ) {
      return;
    }

    t.I( 'comment-replying-to' ).value = '0';
    temp.parentNode.insertBefore( respond, temp );
    temp.parentNode.removeChild( temp );
    cancel.style.display = 'none';
    cancel.onclick = null;
    form.style.maxHeight = "0px";
    formButton.style.display = "";
    t.currentButton.onclick = t.currentFun;

    return false;
  },

  moveForm: function( moveFormButton, commId, parentId, respondId, postId ) {
    var div, element, style, cssHidden,
      t           = this,
      comm        = t.I( commId ),
      respond     = t.I( respondId ),
      cancel      = t.I( 'cancel-comment-reply-link' ),
      parent      = t.I( 'comment-replying-to' ),
      post        = t.I( 'comment-post-id' ),
      form        = t.I("comment-form"),
      formButton  = t.I("form-button"),
      commentForm = respond.getElementsByTagName( 'form' )[0];

    if ( ! comm || ! respond || ! cancel || ! parent || ! commentForm ) {
      return;
    }
    t.currentButton = moveFormButton;
    t.respondId = respondId;
    postId = postId || false;

    if ( ! t.I( 'sm-temp-form-div' ) ) {
      div = document.createElement( 'div' );
      div.id = 'sm-temp-form-div';
      div.style.display = 'none';
      respond.parentNode.insertBefore( div, respond );
    }

    comm.parentNode.insertBefore( respond, comm.nextSibling );
    if ( post && postId ) {
      post.value = postId;
    }
    parent.value = parentId;
    cancel.style.display = '';
    form.style.maxHeight = "800px";
    formButton.style.display = "none";

    

    cancel.onclick = this.cancelReply;
    t.currentFun = t.currentButton.onclick;
    t.currentButton.onclick = this.cancelReply;

    /*
     * Set initial focus to the first form focusable element.
     * Try/catch used just to avoid errors in IE 7- which return visibility
     * 'inherit' when the visibility value is inherited from an ancestor.
     */
    try {
      for ( var i = 0; i < commentForm.elements.length; i++ ) {
        element = commentForm.elements[i];
        cssHidden = false;

        // Modern browsers.
        if ( 'getComputedStyle' in window ) {
          style = window.getComputedStyle( element );
        // IE 8.
        } else if ( document.documentElement.currentStyle ) {
          style = element.currentStyle;
        }

        /*
         * For display none, do the same thing jQuery does. For visibility,
         * check the element computed style since browsers are already doing
         * the job for us. In fact, the visibility computed style is the actual
         * computed value and already takes into account the element ancestors.
         */
        if ( ( element.offsetWidth <= 0 && element.offsetHeight <= 0 ) || style.visibility === 'hidden' ) {
          cssHidden = true;
        }

        // Skip form elements that are hidden or disabled.
        if ( 'hidden' === element.type || element.disabled || cssHidden ) {
          continue;
        }

        element.focus();
        // Stop after the first focusable element.
        break;
      }

    } catch( er ) {}

    return false;
  },

  I: function( id ) {
    return document.getElementById( id );
  }
};