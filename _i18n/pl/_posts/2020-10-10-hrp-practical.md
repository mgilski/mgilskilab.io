---
title: "Jak przetrwać w Pirenejach? Informacje praktyczne"
image: "/assets/images/hrp-practical/cover.jpg"
categories:
- categories.spain
- categories.france
- categories.andorra
- categories.practical
featured: false
---

**Jako, że w Pirenejach po raz pierwszy mieliśmy do czynienia ze szlakiem długodystansowym, bez wątpienia wiele rzeczy mogliśmy zrobić znacznie lepiej. Mimo wszystko, udało nam się ukończyć biegnący przez całe pasmo Haute Randonnée Pyrénéenne (HRP), a przy tym nie zrobić sobie żadnej większej krzywdy. Ten wpis można traktować jako wskazówki dla innych wędrowców, ale też odpowiedź na najczęściej zadawane nam pytania.**

<!-- more -->

## Przygotowanie
Jest to oczywiście kwestia indywidualna, jednak dla nas wyprawa na szlak HRP nie wiązała się z kilkumiesięcznymi przygotowaniami. Podjęliśmy decyzję spontanicznie, plecaki mieliśmy gotowe, kondycję nienajgorszą [uśredniając, bo Maciej jak zawsze świetną, a ja nie jakąś tragiczną - W.]. Pozostało tylko poczytać o przebiegu szlaku oraz spodziewanych warunkach atmosferycznych i mogliśmy ruszać w drogę.

### Dobry termin
Zasadniczo, wysoki sezon w Pirenejach trwa **od połowy czerwca do końca września**. Kto lubi samotność w górach, może pewnie zdecydować się na inne miesiące, jednak szlak HRP staje się wtedy zupełnie innym rodzajem wyzwania. Śnieg zaczyta tu padać szybko, a zalega długo. Wiele przełęczy staje się już jesienią nie do przejścia bez tony sprzętu. Z doświadczenia możemy powiedzieć, że pomiędzy 16 lipca a 2 września warunki na wędrówkę są świetne, a przynajmniej było tak w 2020 roku.

### Dobry sprzęt
Decydując się na wybór/zakup sprzętu na szlak HRP, warto pamiętać o kilku kwestiach.
1. W Pirenejach latem bywa naprawdę gorąco, szczególnie w dolinach, ale powyżej 2000 m n.p.m. temperatura spadała nocami do zaledwie **kilku stopni Celsjusza**.
2. Niskie podejściówki okazały się dla nas idealne. Trzeba jednak mieć na uwadze, że nawet w środku lata zdarzają się fragmenty zasypane śniegiem, więc bez butów za kostkę lub stuptutów trzeba chodzić bardzo ostrożnie.
3. Jeżeli nie jesteś koksem, który przechodzi 800 km w trzy tygodnie albo i mniej (spotkaliśmy kilku takich na trasie), to potrzebujesz **namiotu**, bo na niektórych etapach nie ma innych opcji noclegu. Jak wiemy z opowieści spotkanych wędrowców, 800-gramowe płachty ze stelażem z kijków trekkingowych mogą nie wytrzymać nocnych burz, nawiedzających Pireneje ze sporą częstotliwością.
4. Jeżeli ktoś ma inny patent, to zachęcamy do podzielenia się w komentarzach, jednak naszym zdaniem **nie da się przejść HRP bez ładowarki solarnej**. Z naszych małych paneli zasialiśmy telefon, aparat, czytnik e-booków i powerbanka. Z “normalnego” prądu z gniazdka skorzystaliśmy dosłownie raz.
5. Twój plecak musi pomieścić nie tylko zabrany z domu sprzęt, ale też sporo **jedzenia i wody**. Niezbędne są zapasy nawet na 5-6 dni (znów, jeżeli idziesz spokojnym tempem). Źródeł i innych miejsc na napełnienie butelki przez większość szlaku na szczęście nie brakuje.

{% include post-carousel-folder.html folder="hrp-practical" images="10" alt="Krowa na szlaku HRP" ratio="2 / 3" captions="Ładowarka solarna przyczepiona do plecaka (i Wera)" %}

A co konkretnie zabraliśmy my? **Nie bierzcie przykładu!** W plecakach mieliśmy **wszystko, co zamierzaliśmy zabrać na wyprawę dookoła świata**, czyli mnóstwo niepotrzebnych ubrań i innych gratów, w tym laptopa. Stwierdziliśmy, że w najgorszym wypadku odeślemy nadmiar do Polski, jednak dzięki naszym kochanym, genialnie skonstruowanym plecakom, praktycznie nie narzekaliśmy na konieczność dźwigania tego wszystkiego. Śmiejcie się z nas śmiało, ale Maciej, po wyładowaniu plecaka jedzeniem na tydzień, nosił na plecach dobre 20 kg.

### Dobra mapa
Uwaga! HRP sam w sobie **nie jest znakowany**. Kiedy łączy się z innymi szlakami, można opierać się na oznaczeniach, ale przez większość czasu mapa to podstawa. My opieraliśmy się na **przewodniku Paula Atkinsona**, który pobraliśmy w formie e-booka ([stąd](https://whiteburnswanderings.wordpress.com/2018/12/28/hrp-pocket-guide-rev-1/), jest tam przewodnik w formacie doc i plik gpx). Słowny opis typu “po 2 km skręć w lewo leśną ścieżką” lub co gorsza, “zaimprowizuj drogę w dół”, uzupełniliśmy o dwie aplikacje – mapy.cz i maps.me z wgranym śladem GPS. Tak zestaw sprawdził się bardzo dobrze, bo zgubiliśmy się tylko kilka razy i zawsze wynikało to z zagadania się.

{% include post-carousel-folder.html folder="hrp-practical" images="9" alt="Mapa HRP" ratio="1" captions="Wera sprawdzająca przewodnik" %}

## Życie na szlaku
Podkreślamy, że dzielimy się własnymi doświadczeniami, a nie przedstawiamy gotowy przepis na przejście szlaku HRP. Pamiętajcie proszę czytając o naszych zwyczajach na szlaku, że nie gonił nas żaden termin, a ciężkie plecaki nosiliśmy z wrodzonego uporu.

### Co jeść?
Korzonki? Grzyby? Szyszki? Nie było potrzeby, by przechodzić w tryb survivalowy. Z darów gór korzystaliśmy obficie, ale głównie w postaci malin i jagód, których to rosną w Pirenejach całe łany. Z przewodnika wiedzieliśmy, co ile kilometrów trafia się cywilizacja (czasem co 50, czasem co 100…). Już po pierwszym przystanku, po czterech dniach, wymyśliliśmy świetny system. Zamiast kupować w lokalnych, strasznie drogich i zwykle małych sklepach, robiliśmy sobie pół dnia chilloutu, podczas którego Maciej jechał **autostopem do supermarketu** w większym mieście.

Oboje mamy słabość do dobrego, obfitego jedzenia. Bezpośrednio po zakupach urządzaliśmy sobie prawdziwe uczty, z ciastem, owocami, chipsami i napojami innymi niż woda źródlana. Pomiędzy sklepami odżywialiśmy się głównie orzeszkami ziemnymi, czekoladą oraz kaszą kuskus lub makaronem z rozmaitymi dodatkami. Pod koniec przestaliśmy liczyć gramy, więc nosiliśmy ze sobą mniej zoptymalizowane wagowo, a bardziej różnorodne przekąski, od mleka czekoladowego po żelki.

{% include post-carousel-folder.html folder="hrp-practical" images="5,4" alt="Jedzenie na szlaku HRP" ratio="2 / 3" captions="Mac and cheese - najlepszy obiad na trasie; Super sałatka mieszana w paczce" %}

### Gdzie spać?
Jak wspominaliśmy, **namiot to podstawa**. Rozbijanie go jest jak najbardziej dozwolone, nawet w parkach narodowych, gdzie zakaz biwakowania obowiązuje tylko od 9.00 do 19.00. Ponadto, we wspomnianym przewodniku zaznaczono **ogólnodostępne chatki**. Ich standard jest, delikatnie mówiąc, różny, ale w każdej można liczyć na dach nad głową i prycze do spania. Korzystaliśmy z takiej odmiany od namiotu bodajże pięciokrotnie. Zasadniczo były to spokojne noce, poza jedną, kiedy to stoczyliśmy walkę z armią myszy, zakończoną stratą tabliczki czekolady. Trzecią opcją noclegową są **schroniska**, jednak jest ich względnie niewiele, niemal wszystkie trzeba w czasie wakacji rezerwować z wyprzedzeniem.

{% include post-carousel-folder.html folder="hrp-practical" images="1,7,8" alt="Spanie na szlaku HRP" ratio="2 / 3" %}

### Jak lepiej pachnieć?
Czy higiena na szlaku to temat rzeka? Nie, ale za to rzeka tę higienę może zapewnić. Obok rzeki można dopisać strumyk lub jeziorko. Myliśmy się głównie metodą intensywnego szorowania, praktycznie co by ograniczyć do minimum wzbogacanie ekosystemu mydłem. Mniej lub bardziej cywilizowane łazienki spotykaliśmy w schroniskach oraz w miejscowościach przy szlaku.

Mieliśmy ze sobą na tyle dużo odzieży, że nie musieliśmy prać jej ręcznie w napotkanych zlewach i zbiornikach wodnych. Po prostu po przepoceniu wszystkich ubrań odwiedzaliśmy samoobsługową pralnię, najczęściej przylegającą do supermarketu. Innym popularnym (i lżejszym) sposobem jest branie tylko dwóch zestawów ubrań i pranie na bieżąco.
 
### Jak się porozumieć?
W Pirenejach czuliśmy się komfortowo, bo jedno z nas dogaduje się po **hiszpańsku**, a drugie po **francusku**. Uspokajamy jednak tych, którzy nie mówią w żadnym z tych języków. Niemal każda zagadana osoba z plecakiem znała **angielski** i na pewno łatwo byłoby znaleźć kogoś, kto pomoże w porozumieniu się z obsługą sklepu czy schroniska.

## Pirenejskie niebezpieczeństwa
**Trudno wezwać pomoc**
W tych górach naprawdę nie ma zasięgu. Znacie ze swoich telefonów komunikaty “tylko połączenia alarmowe”? Na szlaku HRP zwykle nie było nawet tego. Były fragmenty, gdzie zasięgu nie było przez pięć bitych dni.

**Słaba widoczność**
Zachodni fragmentu szlaku często spowijają niskie chmury, znacznie utrudniające nawigację. Łatwo nie tylko się zgubić, ale i wpaść wprost na wyłaniającą się z mgły krowę.

{% include post-carousel-folder.html folder="hrp-practical" images="2" alt="Krowa na szlaku HRP" ratio="1" %}

**Nagłe burze**
Przez kilka nocy nasz 6-letni już namiot przechodził, na szczęście pomyślnie, próby wytrzymałości. Burze połączone z wichurą, ulewą i gradobiciem są jednym z największych zagrożeń dla wędrowców.

**Psie kły**
Pasące się konie, krowy i owce są nieodłącznym elementem tutejszego krajobrazu. Pilnują ich zwykle potężne pirenejskie psy górskie, zwane *patou* (samce ważą zwykle 50-60 kg). Jeśli im się nie spodobasz, co raz się nam zdarzyło, potrafią efektownie warczeć i szczekać, a nawet chapnąć paszczą dół plecaka. Ich celem nie jest oczywiście zrobienie krzywdy, tylko przestraszanie. Sprawdzają się w tym zadaniu wzorowo.

{% include post-carousel-folder.html folder="hrp-practical" images="3" alt="Patou na szlaku HRP" ratio="2 / 3" captions="Dwa urocze psy które prawie wystraszyły nas na śmierć 3 minuty przed zrobieniem tego zdjęcia" %}

## Przykładowe ceny
Naszym głównym wydatkiem były zakupy spożywcze w supermarketach. Każdy może sobie sprawdzić, ile kosztuje makaron we francuskim Intermarche (więcej niż w polskim, ale nie złodziejsko więcej), więc o tym pisać nie zamierzamy. Warto wspomnieć jednak o **cenach w schroniskach**, bo do takich informacji nieco trudniej dotrzeć. Co ciekawe, pod względem standardowych wydatków nie zauważyliśmy niemal żadnej różnicy pomiędzy Francją a Hiszpanią.

- nocleg bez wyżywienia – 15-20€
- talerz spaghetti lub danie dnia – 9-10€
- duża kanapka – 5-6€
- kawa – 1,5-3€
- kawałek ciasta – 3-5€

\* \* \*

Spędziliśmy w Pirenejach 47 dni, docierając od Oceanu Atlantyckiego (Hendaye) do Morza Śródziemnego (Banyuls-sur-Mer). Szliśmy bez pośpiechu, robiąc długie przerwy kiedy tylko mieliśmy na to ochotę. Opisane informacje praktyczne dotyczą zatem typowo rekreacyjnego przejścia szlaku HRP. Nie mamy niestety jak doradzić, czy potrzebne są dwie czy trzy koszulki techniczne, albo ile kalorii na 100 g mają najlepsze batoniki na drogę. Niemniej jednak, trzymamy kciuki za wszystkich, którzy idą w Pireneje po sportowy wyczyn!

**Jeśli chcecie obejrzeć zdjęcia z naszej wyprawy, koniecznie odwiedźcie nasz [post z foto-galerią ze szlaku HRP](/posts/hrp-gallery)!**
