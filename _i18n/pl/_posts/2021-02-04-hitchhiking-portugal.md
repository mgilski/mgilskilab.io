---
title: "Autostopem przez Portugalię"
image: "/assets/images/hh-portugal/cover.jpg"
categories:
- categories.portugal
- categories.practical
featured: false
---


**Na ten kraniec Europy dotarliśmy w 2020 roku pierwszy raz, a biorąc pod uwagę negatywne doświadczenia autostopowe z sąsiedniej Hiszpanii i koronawirusa gdzieś w tle, nie robiliśmy sobie nadziei, że podróż będzie ekspresowa. Niemniej jednak, podjęliśmy wyzwanie i poszło znacznie lepiej, niż się spodziewaliśmy. Jeśli chcesz się dowiedzieć, jak działa autostop w Portugalii (przynajmniej z naszego doświadczenia), to zachęcamy do lektury.**

<!-- more -->

## W skrócie
**Dni**: 13 (z tego może 3-4 faktycznie w trasie) \\
**Przejechany dystans**: 1100 km \\
**Kierowcy**: 22 \\
**Najdłuższy dystans przejechany jednym autem**: 460 km (Ourique-Porto) \\
**Najkrócej czekaliśmy**: 0 minut (kierowca zatrzymał się, kiedy nawet nie łapaliśmy stopa) \\
**Najdłużej czekaliśmy**: 3 godziny \\
**Mediana**: 40 minut

## Nasza trasa
Jakkolwiek by to nie zabrzmiało, wylądowaliśmy w portugalskim Faro z powodów czysto praktycznych. Ewakuując się z Ameryki Południowej wybraliśmy najtańszy z niewielu dostępnych lotów z São Paulo. Więcej o lockdownie w Paragwaju i trudnym powrocie do Europy napisaliśmy tutaj. 
Dodając do tego fakt, że nie mieliśmy wcześniej okazji odwiedzić Portugalii, wybór tym bardziej był prosty.

Z Faro wyruszyliśmy w trasę wzdłuż wybrzeża, do Przylądka św. Wincentego, podziwiając po drodze ciekawe zakątki regionu Algarve. Następnie przejechaliśmy bez większych przystanków przez niemal całą Portugalię, docierając do Porto. Ostatnim punktem na naszej trasie było już znacznie mniej znane miasto Aveiro, z przyległym kurortem Costa Nova. Autostrada A25 doprowadziła nas do granicy z Hiszpanią.

Nie licząc komunikacji miejskiej w Porto, cały dystans pokonaliśmy autostopem.

{% include post-carousel-folder.html folder="hh-portugal" images="6" alt="Łapanie stopa w portugalii" ratio="1 / 1" %}

## Autostop w Portugalii – nasze odczucia
### Znajomość idei autostopu
Pod tym względem Portugalia nie wyróżniała się spośród większości europejskich państw – kierowcy rozumieli gest wyciągniętego kciuka, nikt nie prosił o zapłatę za przejazd ani nie nalegał, że podwiezie nas na dworzec kolejowy. Nie widzieliśmy innych autostopowiczów, jednak na pewno nie bez znaczenia był generalny spadek ruchu turystycznego w 2020 roku.

### Kierowcy
Mieszkańcy kraju stanowili nieco ponad połowę kierowców, z którymi podróżowaliśmy po Portugalii i **zdecydowana większość mówiła po angielsku**, co znacznie ułatwiło komunikację. W Algarve kilkukrotnie zabraliśmy się z turystami z rozmaitych części świata. Szczególnie zapadło nam w pamięć dwóch gości, którzy zatrzymali się dla nas, mimo, że nawet nie zaczęliśmy łapać stopa, tylko szliśmy wzdłuż drogi. Jeden z nich przyjechał z Niemiec, drugi ze Stanów, by spędzić lato w jednej tutejszych hipisowskich miejscówek

### Miejsca do łapania
Stosunkowo często musieliśmy maszerować dość daleko poza miasto, bo brakowało bezpiecznych miejsc do zatrzymania się. By wydostać się z Porto, dojechaliśmy autobusem do ronda poleconego na Hitchwiki, ale szczerze mówiąc nie było ono jakieś super, więc nie polecamy go dalej.
Wszystkich, którzy chcą spróbować pojeździć autostopem po Portugalii, ostrzegamy przed stacjami benzynowymi, szczególnie przy autostradzie A25. Chyba trzykrotnie trafiliśmy na takie, gdzie praktycznie nikt się nie zatrzymywał. Szczególnie odradzamy Galp za wyjazdem z Aveiro oraz Galp na wysokości Lajeosa do Mondego (niedaleko Guardy).

{% include post-carousel-folder.html folder="hh-portugal" images="5" alt="Łapanie stopa w portugalii" ratio="2 / 3" %}

### Tempo przemieszczania się
Trasę wzdłuż wybrzeża Algarve pokonaliśmy w ślimaczym tempie, ale nie z powodu kiepskiego działania autostopu, tylko z wyboru. W praktyce przemieszczaliśmy się od miasteczka do miasteczka, nie czekając więcej niż 20-30 minut. Dalej było już niestety gorzej i do Porto dojechaliśmy sprawnie tylko dzięki temu, że po praktycznie całym dniu czekania przeplatanego krótkimi odcinkami jazdy, trafił nam się kierowca jadący wprost do Porto

## Ogólna ocena:

{% include post-thumbs.html rating=4 %}

Naszym zdaniem autostop w Portugalii działa o niebo lepiej niż w Hiszpanii. Przy ogólnej ocenie wzięliśmy poprawkę na fakt, że mimo bardzo niewielu przypadków koronawirusa w czasie naszej podróży, mniej osób przemieszczało się na większe dystansie. Inni, co jak najbardziej zrozumiałe, unikali po prostu kontaktu z obcymi ludźmi.
Na pewno chcielibyśmy ponownie odwiedzić Portugalię i przekonać się, jak się po niej podróżuje stopem w normalnych czasach.

## Spanko
W Porto zatrzymaliśmy się u kumpla ze studiów, ale co do zasady **spaliśmy w namiocie**. Szukanie ustronnych miejsc na rozbicie to nie stanowiło nigdzie większego problemu, a w Algarve było nawet atrakcją samą w sobie. Spędziliśmy kilka nocy na klifach, osłonięci roślinnością czy też skałami, a jedną na niewielkiej plaży, zupełnie na yolo, pod gołym niebem.

{% include post-carousel-folder.html folder="hh-portugal" images="1" alt="Spanie na dziko w portugalii" ratio="2 / 3" %}

## Jedzenie
### Zakupy
Po czterech miesiącach w Ameryce Południowej stęskniliśmy się za Lidlem, Aldim, Intermarche… Serio, pierwsza wizyta w supermarkecie wyposażonym w produkty dobrze znane nam z domu autentycznie nas uszczęśliwiła. Ceny są troszkę wyższe niż Polsce, jednak do hiszpańskich i francuskich nadal im daleko.

### Jedzenie na mieście
Głównie gotowaliśmy sami, korzystając z naszego niezawodnego palnika. Z knajp zapadła nam w pamięć właściwie tylko **Almada Cafe** w centrum Porto, gdzie zjedliśmy pyszne *bolinhos de bacalhau* (klopsiki z dorsza i ziemniaków) i *francesinhę* (typowa dla Porto zapiekanka z chleba i absurdalnej ilości mięsa w pomidorowo-piwnym sosie).

{% include post-carousel-folder.html folder="hh-portugal" images="4,3" alt="Jedzenie w portugalii" ratio="2 / 3" captions="Francesinha w Almada Cafe; Pastéis de nata" %}

## Bezpieczeństwo
Właściwie w całej Europie, a na pewno w jej unijnej części, czujemy się “u siebie”, a co za tym idzie, permanentnie towarzyszy nam błogie poczucie bezpieczeństwa. Podczas tych dwóch tygodni jedyną sytuacją, podczas której odczułam dyskomfort [W.], było spanie pod namiotem przy płocie odgradzającym teren łowiecki.

\* \* \*

Wyprawa autostopem przez Portugalię minęła nam bardzo szybko i przyjemnie. Bez wahania polecamy ją wszystkim miłośnikom alternatywnych form podróżowania. My na pewno wrócimy, chociażby do Lizbony, którą nadal znamy tylko ze zdjęć. 
