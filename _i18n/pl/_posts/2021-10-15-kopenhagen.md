---
title: "Co warto zobaczyć w Kopenhadze? Jeden dzień w stolicy Danii"
image: "/assets/images/kopenhagen/3.jpg"
feature_text: "Opis naszych poprzednich podróży"
categories:
- categories.denmark
- categories.articles
featured: false
---

**Syrenkę mamy też w Warszawie, kanały w Amsterdamie, a tak w ogóle, to ze skandynawskich stolic ciekawszy wydawał mi się zawsze Sztokholm. W takim razie, czy warto jechać do Kopenhagi? A jeśli tak, czy jest sens zatrzymywać się na jeden dzień? Spieszę z moją, oczywiście nad wyraz subiektywną, odpowiedzią.**

<!-- more -->

## Ogólne wrażenia
Jeden dzień może się wydawać niewystarczający na zwiedzanie Kopenhagi, ale tak mi się ułożyło, że trafiłam tam w ramach **study touru** i właśnie tyle czasu przewidzieli organizatorzy. Z perspektywy czasu uważam, że pewnie fajniej byłoby zostać dłużej, żeby zobaczyć jeszcze kilka muzeów i zamek Rosenborg. Niemniej jednak, doba wystarczyła na konkretny spacer szlakiem wszystkich ikonicznych miejsc w Kopenhadze. Zwiedzanie z przewodniczką, w połączeniu z wieczornym włóczeniem się po totalnie nieturystycznej części miasta, zostawiło mnie z przekonaniem, że w stolicy Danii na pewno dobrze by się mieszkało, za to nie nazwałabym jej turystycznym *must-see*.

## Co warto zobaczyć w Kopenhadze w jeden dzień?
Kopenhaga liczy z osiemset tysięcy mieszkańców, więc trudno ją nazwać małym miastem. Na szczęście dla osób, które preferują zwiedzać na piechotę (a do takich zdecydowanie się zaliczam), wszystkie najciekawsze, a przynajmniej najsłynniejsze, miejsca i zabytki znajdują się w spacerowym zasięgu od centrum.

### Nyhavn
Kiedy sama się zastanawiałam, co warto zobaczyć w Kopenhadze, do głowy przychodził mi przede wszystkim obraz wąskich, kolorowych kamieniczek przycupniętych nad kanałem. Troszkę się zawiodłam, bo wyglądała tak tylko jedna ulica – właśnie Nyhavn. Nie dziwię się, że miasto zrobiło sobie z tego miejsca wizytówkę. Wiąże się z nim wyjątkowa historia, bo chociaż dzisiaj to przede wszystkim ostoja horrendalnie drogich knajp, jeszcze jakieś 70 lat temu Nyhavn słynęło głównie z typowych portowych tawern i domów publicznych.

{% include post-carousel-folder.html folder="kopenhagen" images="2" alt="Zabytki w kopenhadze" ratio="4 / 3" %}

### Syrenka i okolice
Można by powiedzieć, że być w Kopenhadze i nie zobaczyć pomnika Małej Syrenki, to jak być w Paryżu i przegapić Wieżę Eiffla. Tylko że nie do końca. Wieżę widać zewsząd, natomiast do Syrenki trzeba się pofatygować. Czy ma to jakiś sens? Pewnie tak, ale tylko ze względu na symbolik. Rzeźba sama w sobie nie powala. Jest niewielka, permanentnie otoczona przez turystów, a do tego w tle widać spalarnię śmieci.

{% include post-carousel-folder.html folder="kopenhagen" images="6" alt="Zabytki w kopenhadze" ratio="4 / 3" %}

### Amalienborg
Duńska rezydencja królewska to kolejny obowiązkowy przystanek. Jest ona o tyle ciekawa, że składają się na nią cztery identyczne pałace. Klimatu dodają gwardziści w tradycyjnych mundurach i tych śmiesznych futrzanych czapkach (bermycach), które noszą też strażnicy pod Buckingham. Obok Amalienborga znajduje się też Kościół Marmurowy, który trudno przegapić, ze względu na okazałą zieloną kopułę.

{% include post-carousel-folder.html folder="kopenhagen" images="5" alt="Zabytki w kopenhadze" ratio="4 / 3" %}

### Rejs po kanałach
Zwiedzanie Kopenhagi można urozmaicić rejsem po kanałach przecinających historyczne centrum. Podczas 45-minutowej wycieczki, w której wzięłam udział, zobaczyliśmy z perspektywy łodzi między innymi Nyhavn i Małą Syrenkę. Na pokładzie był przewodnik, opowiadający o mijanych miejscach po duńsku i angielsku, ale nagłośnienie sprawdzało się fatalnie, więc sporo trzeba było sobie dopowiadać.

{% include post-carousel-folder.html folder="kopenhagen" images="4" alt="Zabytki w kopenhadze" ratio="2 / 3" %}

### Christiania
Wolne Miasto Christiania to taki wycinek Kopenhagi, któremu w latach 70. przyznano pewną dozę niezależności. Przez całe lata państwowe prawo nie obowiązywało tutaj w pełni, w szczególności przymykano oko na handel narkotykami. Czy “porządny turysta” ma po co odwiedzać Christianię? Moim zdaniem nie zaszkodzi wstąpić na krótki spacer, choćby ze względu na street-art. Jeśli jednak spodziewasz prawdziwej hippisowskiej mekki, pewnie odstraszą cię kramy z koszulkami i gadżetami. W biały dzień czułam się tam zupełnie bezpiecznie. Trudno było przegapić sporadyczne osoby w stanie ewidentnego upojenia czy unoszący się w powietrzu zapach marihuany, ale to przecież część legendy tego miejsca.

## Mały smaczek, czyli odkrycie wieczorową porą
Na koniec skromna anegdota, którą bez wątpienia najbardziej zapamiętam z pobytu w Kopenhadze. Pierwszego wieczoru wybrałam się z koleżanką na spacer po dzielnicy **Nørrebro**, gdzie nocowałyśmy. Żeby nie włóczyć się zupełnie bez celu, jako końcowy punkt wybrałyśmy sobie najbliższy spory fragment zieleni na mapie. Kiedy już przekroczyłyśmy bramę parku, najpierw zaskoczyły nas pojawiające się tu i ówdzie wiekowe nagrobki, potem tabliczki opatrzone strzałkami i nazwiskiem **“Hans Christian Andersen”**. Tak, zupełnym przypadkiem trafiłyśmy na najsłynniejszy cmentarz w Danii, a do tego tuż przed zamknięciem odnalazłyśmy grób autora tony pięknych baśni. 
Nie ma to jak zdać się na ślepy traf i po prostu kręcić się po mieście!

## Informacje praktyczne
- w Kopenhadze moja grupa nocowała w hotelu **A&O**. Jak ktoś nie zna, to jest to niemiecka sieć ok. 40 hoteli i hosteli w całej Europie. Do tej pory kilka razy korzystałam z ich oferty. Standard wszędzie podobny, czyli bez luksusów, za to czysto i wszystko ładnie urządzone.  Bardzo dobry stosunek ceny do jakości
- tym razem jadałam w hotelu, ale z przyzwyczajenia zwracałam uwagę na **ceny w knajpach**. Różnica między centrum, a chociażby tą naszą dzielnicą Nørrebro wydała mi się astronomiczna. Nie daj sobie wmówić, że w Kopenhadze wszystko kosztuje fortunę. Oddal się od turystów i szukaj kebabów/falafli za 30-35 DKK
- po mieście najłatwiej przemieszczać się **komunikacją miejską** (4 linie metra i liczne autobusy) albo **rowerem**. Kto nie ma własnego, może wypożyczyć. Wystarczy założyć konto na bycyklen.dk i płacić 12 DKK za każde 20 minut
