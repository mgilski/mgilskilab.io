---
title: "7 rzeczy, które zaskoczyły nas w Paragwaju"
image: "/assets/images/paraguay-facts/cover.jpg"
categories:
- categories.paraguay
- categories.thoughts
featured: false
---


**W czasie dwóch miesięcy w Paragwaju niewiele rzeczy nas zachwyciło, za to sporo zdziwiło. To pierwszy pozaeuropejski kraj, w którym przyszło nam spędzić wspólnie tak dużo czasu, więc z tym większym zaciekawieniem obserwujemy tutejszą rzeczywistość (oczywiście w miarę ograniczonych przez pandemię możliwości).**

<!-- more -->

## 1. Dwujęzyczność
Przed przyjazdem wiedzieliśmy z Wikipedii, że tutejszym językiem urzędowym jest, obok hiszpańskiego, **guarani**. Skalę jego znajomości odkryliśmy jednak dopiero na miejscu, dopytując o to Paragwajczyków. Guarani posługują się nie tylko osoby pochodzenia indiańskiego i mieszanego, ale również potomkowie imigrantów. Łącznie to **90% ludności**! Żeby nie było wątpliwości – pozostała część używa po prostu innych rdzennych języków, bo znajomość hiszpańskiego wynosi ok. 87%. 

## 2. Yerba mate niczym woda
Zapowiedź tego zjawiska mieliśmy już w argentyńskiej prowincji Misiones, jednak nie na darmo yerba mate nazywa się oficjalnie **“ostrokrzew paragwajski”**. Mieszkańcy kraju zdają się pić napar z tej rośliny od świtu do nocy. Dosłownie wszędzie nosi się tu ze sobą termos, który umożliwia systematyczne zalewanie kolejnych porcji. Bardzo popularna jest mate przygotowana na zimno, czyli **tereré**. Rząd gorąco apeluje, by w czasie epidemii koronawirusa zrezygnować ze zwyczaju podawania napoju z rąk do rąk i picia przez wspólną bombillę (metalową rurkę zakończoną sitkiem).

{% include post-carousel-folder.html folder="paraguay-facts" images="3" alt="Yerba mate w Paragwaju" ratio="1" %}

## 3. Sopa paraguaya
Do tutejszych narodowych potraw należy sopa paraguaya. Zadziwia w niej to, że zupą [hiszp. *sopa*] jest wyłącznie z nazwy. W rzeczywistości to coś w rodzaju gąbczastej zapiekanki z mąki kukurydzianej, jajek, mleka, cebuli i sera. Skąd zatem ta zupa? Według legendy, danie “wynalazł” przypadkiem kucharz, który z wymienionych składników faktycznie chciał przygotować danie w formie płynnej, ale zdecydowanie przesadził z mąką.

## 4. Nadprodukcja folii
Tu bardzo negatywne zaskoczenie dla takich gringos jak my. **Kultura ograniczania plastiku** ewidentnie nie dotarła do paragwajskich sklepów. Jeżeli zawczasu nie zaprotestujesz, obsługa na 100% zapakuje Twoje zakupy do reklamówki. Rekord pod tym względem pobiła sprzedawczyni, która zaoferowała nam foliową torebkę przy zakupie pojedynczego artykułu, którym były… orzeszki na wagę w foliowej torebce.

## 5. Dysproporcje w cenach produktów
Przyjechaliśmy do Paragwaju z nastawieniem, że będzie tu naprawdę tanio, bez porównania z Brazylią. Nie wiem, gdzie robili zakupy autorzy tych opinii, ale my w obu krajach wydawaliśmy na jedzenie mniej więcej tyle samo. Rzeczywiście niektóre artykuły spożywcze mają przyjemne ceny. Można sporo zaoszczędzić kupując takie produkty jak mąka, ryż, makaron czy kukurydza **na wagę**. Płatki śniadaniowe, czekoladę, zdarzało nam się kupować taniej w takich krajach jak Wielka Brytania czy Francja, a markowe produkty importowane typu nutella kosztują nawet 4x więcej niż w Polsce.

## 6. Everyday Is Christmas
Ogromne choinki na rondach? Łańcuchy z dzwoneczkami nad wejściem? Zawieszki-renifery na drzwiach? Wszystko to widzieliśmy w Paragwaju **w kwietniu** i nie mamy wątpliwości, że sytuacja pozostaje bez zmian przez cały rok. Spytaliśmy o to kierowcę, który podwiózł nas do Encarnación i otrzymaliśmy oczywistą odpowiedź, że nie ma sensu zdejmować dekoracji, skoro za kilka miesięcy znów trzeba by zawieszać. Cóż, Paragwajczykom zdecydowanie nie można odmówić **wysokiego poziomu chillu**.

{% include post-carousel-folder.html folder="paraguay-facts" images="1,2" alt="Chiristams in Paraguay" ratio="1" %}

## 7. Wygodne spanie
Płacąc 16 zł za nocleg, nie ma sensu narzekać na brudne ściany z odchodzącą farbą, przedpotopowe meble czy brak okna. Zaskoczyło nas natomiast, że bez względu na standard pozostałej części pokoju, we wszystkich miejscach, w których mieliśmy okazję spać, były naprawdę wyjątkowo wygodne materace. Trudno powiedzieć, dlaczego właściciele inwestują akurat w ten element wyposażenia, ale nam to odpowiada, bo nie ma to jak dobre spanko po aktywnym dniu.

{% include post-carousel-folder.html folder="paraguay-facts" images="4" alt="Spanie w Paragwaju" ratio="1" %}