---
title: "Autostopem przez Brazylię"
image: "/assets/images/hh-brazil/10.jpg"
categories:
- categories.brazil
- categories.practical
- categories.articles
featured: false
---

**Brazylia to 42. państwo, w którym wypróbowaliśmy działanie autostopu. 3 tygodnie to zdecydowanie za mało, żeby zobaczyć tak ogromny, różnorodny kraj, więc ograniczyliśmy się do stanów São Paulo, Rio de Janeiro i Paraná. Relacja z naszych autostopowych doświadczeń dotyczy tylko tego obszaru.**

<!-- more -->

## W skrócie

**Dni**: 21  
**Przejechany dystans:** 2 000 km  
**Kierowcy:** 22  
**Najdłuższy dystans przejechany jednym autem:** 290 km (Paraty-São Paulo)  
**Najkrócej czekaliśmy:** 5 minut  
**Najdłużej czekaliśmy:** 4 godziny  
**Mediana:** 50 minut

## Nasza trasa

Przylecieliśmy z Europy na **lotnisko Guarulhos** na przedmieściach São Paulo, skąd ruszyliśmy do **Rio de Janeiro** autostradą BR-116 (Rodovia Presidente Dutra). Po kilku karnawałowych dniach wróciliśmy na trasę, tym razem jadąc wzdłuż wybrzeża **Costa Verde** do miasteczka **Paraty**, a następnie do **São Paulo**. W stanie Paraná zatrzymaliśmy się w **Kurytybie** oraz w **Foz do Iguaçu**.

Poza poruszaniem się po miastach, nie korzystaliśmy z innego środka transportu niż autostop.

{% include post-carousel-folder.html folder="hh-brazil" images="map" alt="Mapa podróży po Brazylii" ratio="2 / 3" %}

## Autostop w Brazylii – nasze odczucia

### Znajomość idei autostopu
Nie spotkaliśmy innych autostopowiczów, ale kierowcy ewidentnie rozumieli, co mamy na myśli stojąc przy drodze z wyciągniętym kciukiem, o kartonie nie wspominając. Wiele napotkanych osób kojarzyło autostop w Brazylii z **policjantami i strażakami**, którzy chętnie wracają w ten sposób z pracy. Faktycznie udało nam się zaobserwować to zjawisko!

Kilkukrotnie zatrzymywali się dla nas taksówkarze, a jeszcze częściej kierowcy UBER-a, jednak wystarczyło użyć słowa *carona* (przejażdżka, pot. autostop), żeby zrozumieli nasze intencje. Dwukrotnie zaproponowano nam krótki, darmowy przejazd, więc tłumaczenie się opłaciło.

{% include post-carousel-folder.html folder="hh-brazil" images="10" alt="Ozdoby lokalnych ciężarówek" ratio="2 / 3" %}

### Kierowcy
Z jednym wyjątkiem, zabierali nas Brazylijczycy i Brazylijki, a nie zagraniczni turyści. Zdecydowanie najczęściej jeździliśmy **samochodami osobowymi**, ale trafiły się też cztery ciężarówki, jeden camper i jeden… autobus szkolny.

Wszyscy kierowcy byli sympatyczni, chociaż kontakt znacznie utrudniała **bariera językowa**. Znaczy oni rozumieli świetnie nasze łamane hiszpańskie hablanie, ale dla nas portugalski brzmiał na początku jak mowa kosmitów. Na szczęście po drodze przywykliśmy do wymowy i, z małą pomocą Google Translate, rozmowy szły wcale nie najgorzej. 

Czytaliśmy, że niewielu Brazylijczyków poza Rio de Janeiro i São Paulo mówi po angielsku. Nasza poprawka: w tych miastach również takich ludzi trzeba szukać ze świecą. Zdecydowanie najwięcej anglojęzycznych mieszkańców spotkaliśmy w Kurytybie. Znajomości języków obcych przez kierowców oceniamy na mocne 1/10.

{% include post-carousel-folder.html folder="hh-brazil" images="3,1" alt="Psi towarzysze podróży" captions="Pies Zeus; Adoptowane dzieci Zeusa" ratio="2 / 3" %}

### Miejsca do łapania
Dobre miejsca do łapania zależą zwykle od klasy drogi i autostop w Brazylii nie różni się pod tym względem od innych znanych nam krajów.
- **autostrady** - stacje benzynowe są rozmieszczone średnio gęsto, większość z nich ma jeden wyjazd, co ułatwia łapanie stopa. Byliśmy bardzo pozytywnie zaskoczeni czystością łazienek oraz całkiem powszechną obecnością prysznicy. Autostrady są płatne, więc łapać można również za punktami poboru. Często stacjonuje tam policja, co nie przeszkadza, a wręcz może pomóc
- **inne drogi** - nie podróżowaliśmy przez żadne bezdroża, więc mowa tylko o dobrze utrzymanych drogach międzymiastowych. Najlepszymi miejscami do łapania wydały nam się zatoczki dla autobusów, ale większość przejechanych przez nas odcinków miała szerokie pobocza, gdzie również dało się bezpiecznie zatrzymać.

{% include post-carousel-folder.html folder="hh-brazil" images="2,4" alt="Brazylijskie drogi" ratio="2 / 3" %}


### Tempo przemieszczania się
Niby nigdzie nam się nie spieszyło, ale nie narzekalibyśmy, gdyby jazda autostopem po Brazylii szła nieco szybciej. Nierzadko czekaliśmy powyżej godziny, a często z jednym kierowcą pokonywaliśmy tylko 10-20 km.

Nasz dotychczasowy rekord dobowy to prawie 1500 km (Orlean-Poznań), natomiast w Brazylii żadnego dnia nie przekroczyliśmy 400 km.

## Ogólna ocena:

{% include post-thumbs.html rating=3 %}

Spotkaliśmy całe mnóstwo kochanych ludzi, którzy widząc nas, czyli parę spoconych białasów z plecakami, oferowali nam rady, wodę, przekąski, a w dwóch przypadkach nawet nocleg. Tym bardziej zdziwiliśmy się, dlaczego autostop w Brazylii działa co najwyżej **tak sobie**.

Przypisujemy to głównie powszechnemu **strachowi**. Brazylijczycy sami uważają swój kraj za niebezpieczny, a wzięcie do swojego samochodu obcego człowieka może być uważane za zbędne ryzyko. 



## Spanko

### Tanie zakwaterowanie
Bardzo tanie noclegi o przyzwoitym standardzie łatwo znaleźć na Airbnb. Nie mieliśmy zbyt wielu okazji do włóczenia się po miastach w poszukiwaniu noclegu, ale kilka razy mignęły nam ceny rzędu 50 BRL od osoby, więc na pewno da się znaleźć sensowny nocleg również bez rezerwacji.

### Spanie na dziko
Większość nocy spędziliśmy w namiocie, śpiąc, jak to w trasie, gdzie popadnie. Momentami znalezienie dostępu do kawałka płaskiego terenu nie było łatwe, bo część Brazylii, którą odwiedziliśmy, porośnięta jest bardzo **gęstą, tropikalną roślinnością**. Zdarzyło nam się rozbijać namiot przy stacjach benzynowych i obsługa nie zwracała na nas uwagi.

{% include post-carousel-folder.html folder="hh-brazil" images="12" alt="Namiot w tropikalnym klimacie" ratio="3 / 2" %}

## Jedzenie
### Zakupy
Podczas pobytu w Brazylii regularnie mieliśmy dostęp do kuchni, a do tego mamy na wyposażeniu butlę z gazem i palnik, więc udało nam się trochę pogotować.

Ceny podstawowych produktów spożywczych nie odbiegają zbytnio od polskich. Wyjątki:
- **bardzo tanie:** mango, zielona papryka (inne kolory kosztują o wiele więcej), jajka, napoje gazowane (niemarkowe)
- **bardzo drogie:** pieczywo inne niż chleb tostowy, kasza kuskus, czekolada, chipsy, masło orzechowe, większość nabiału

### Jedzenie na mieście
W Brazylii je się poza domem w porze lunchowej, kiedy to królują knajpy *por kilo*, z jedzeniem na wagę, a czasem również z opcją all you can eat. Wolimy jeść główny posiłek dnia wczesnym wieczorem, więc z tej przyjemności skorzystaliśmy tylko raz czy dwa. Spróbowaliśmy za to wielu popularnych tutaj przekąsek.

{% include post-carousel-folder.html folder="hh-brazil" images="5,6,8,9,11" alt="Brazylijskie przekąski" captions="Salgado + refresco - klasyczny zestaw do kupienia od ulicznych sprzedawców, czyli słona przekąska i zimny napój; Churros - zwykle do wyboru z czekoladą albo dulce de leche; Coxinha - chrupiące pierożki z kurczakiem i dodatkami; Pão de queijo - bułeczki serowe z mąki z manioku; Pastel - prostokątne ciastko z nadzieniem, zazwyczaj mięsnym, smażone na głębokim tłuszczu" ratio="2 / 3" %}

Na podium najsmaczniejszych dań umieszczamy kanapki i żeberka z Estadão w São Paulo oraz naleśniki z tapioki zrobione przez naszą brazylijską mamę Patricię ❤

{% include post-carousel-folder.html folder="hh-brazil" images="7" alt="Kolacja w Estadão" ratio="2 / 3" %}

## Bezpieczeństwo
O kwestiach bezpieczeństwa w Brazylii napisało już wielu podróżników znacznie bardziej doświadczonych niż my. W dużych miastach wyjątkowo łatwo pożegnać się z portfelem, aparatem, albo i gorzej.

Zdrowego rozsądku nam, wbrew pewnym pozorom, nie brakuje. Staraliśmy się nie włóczyć po nocach, mijaliśmy fawele szerokim łukiem, nie afiszowaliśmy się ze sprzętem elektronicznym itp. Nie zetknęliśmy się z żadnym przejawem przestępczości, ale nie zmienia to faktu, że sporadycznie dotyka ona turystów.

Trzeba przyznać, że Brazylijczycy samo nakręcają atmosferę zagrożenia. Prawie codziennie słyszeliśmy zdania rodzaju “Skoro jedziecie do Rio, to nigdzie nie wychodźcie wieczorem”, “Nie boicie się?”, “Lepiej schowaj ten telefon”. To ostatnie zostało wypowiedziane, kiedy w środku dnia staliśmy obok kościoła, osłonięci z dwóch stron ścianami, z grupą policjantów w zasięgu wzroku. Takie podsumowanie.
