---
title: "Gdańsk - murale na Zaspie"
image: "/assets/images/zaspa/5.jpg"
categories:
- categories.poland
- categories.practical
featured: false
---


**Zdaniem naszych i wielu innych podróżników, Gdańsk należy do najciekawszych miast w Polsce. Powstały już pewnie całe tysiące materiałów o głównych atrakcjach i propozycjach tras zwiedzania, dlatego my skupimy się po prostu na miejscu, którego próżno szukać na pierwszych stronach przewodników, a bardzo przypadło nam do gustu. Przenieśmy się razem do dzielnicy Zaspa.**

<!-- more -->

Pomimo wielu wcześniejszych wizyt w Gdańsku [w moim przypadku kilka razy w roli pilotki wycieczki - W.], nigdy nie oddalaliśmy się zbytnio od rejonu Głównego Miasta. Może z wyjątkiem wypadu do Oliwy. Ostatnim razem zaczęliśmy nie od zabytków, a od blokowiska. Sama dzielnica Zaspa od początku wydała nam się przyjemna. Budynki są tu ogromne, ale znacznie od siebie oddalone i otoczone zielenią. Niemniej jednak, przyjechaliśmy tu nie ze względu na uroki architektury z lat 80., tylko żeby zobaczyć skrywane przez Gdańsk murale.

## Skąd się wzięły murale na Zaspie?
Słowem wstępu, ta dzielnica, a właściwie dwie dzielnice, Zaspa-Młyniec i Zaspa-Rozstaje, została wzniesiona na terenie dawnego lotniska. O tym nietypowym położeniu przypominają dzisiejsze nazwy ulic, np. Pilotów, Startowa. Pierwsze murale na Zaspie namalowano w 1997 roku, z okazji 1000-lecia Gdańska. Inicjatywa rozkręciła się na dobre po 2008 roku, kiedy to niejaki Piotr Szwabe uhonorował Lecha Wałęsę, ozdabiając ścianę bloku jego twarzą w wydaniu spikselowanym. Swoją drogą, wybór nie był przypadkowy, bo lider Solidarności mieszkał przez jakiś czas w tym budynku.
O kolejne murale Gdańsk wzbogacił się w ramach odbywającego się tutaj festiwalu **Monumental Art** oraz intensywnych działań **Gdańskiej Szkoły Muralu**. W efekcie, Zaspa może się pochwalić obecnie aż 60 wielkoformatowymi dziełami.

## Spacer szlakiem murali
Kilkadziesiąt obiektów w Kolekcji Malarstwa Monumentalnego może brzmieć przytłaczająco. Na szczęście murale na Zaspie ma pod opieką Instytut Kultury Miejskiej, który nie tylko organizuje regularne zwiedzanie z lokalnymi przewodnikami, ale również przygotował świetną **broszurę i mapkę** (link w informacjach praktycznych na dole) do samodzielnego odkrywania dzielnicy. Poruszaliśmy się zgodnie z zaproponowaną numeracją, więc możemy potwierdzić, że jest bardzo logiczna. Nie spodziewaliśmy się, że przejdziemy całość, ale o dziwo nie znudziło nam się i faktycznie przeszliśmy praktycznie wszystko. Z jednego dnia przeznaczonego na Gdańsk murale zajęły nam **2,5 godziny**, czyli niemałą część.

## Murale na Zaspie – subiektywne top 10
Te prace najbardziej utkwiły nam w pamięci. Kolejność chronologiczna, a tym samym zgodna z trasą wyznaczoną przez Instytut Kultury Miejskiej.

### Piotr Szwabe – *Lech Wałęsa* (Pilotów 17f)
Jeżeli chodzi o murale Gdańsk nie ma chyba słynniejszego (a jeśli ma, poprawcie nas). Nie jest na pewno najbardziej zaawansowany, ale robi wrażenie i w ciekawy sposób uświadamia, że pewne wizerunki są naprawde głęboko wbite w nasze umysły.

{% include post-carousel-folder.html folder="zaspa" images="1" alt="Murale na zaspie" ratio="4 / 3" %}

### Zosen Bandido – *bez tytułu* (Pilotów 18E) 
To jedna z najbardziej kolorowych prac, która od razu zachwyciła nas swoim bajkowym stylem. Ktoś mógłby powiedzieć, że całość prezentuje poziom przedszkolaka, ale czy to aby na pewno źle?

{% include post-carousel-folder.html folder="zaspa" images="3,2" alt="Murale na zaspie" ratio="4 / 3" %}

### Rafał Roskowiński, Jacek Zdybel – *bez tytułu* (Pilotów 16f)
Spośród kilku murali z motywami samolotów, nawiązujących do lotniczej przeszłości Zaspy, ten spodobał nam się najbardziej. Nie jest może oryginalny, ale ujął nas jego komiksowy styl. Inspiracją dla autorów była malarka Tamara Łempickia.

{% include post-carousel-folder.html folder="zaspa" images="4" alt="Murale na zaspie" ratio="4 / 3" %}

### Mazu Prozak – *bez tytułu* (Pilotów 14a)
Kolejny mural uderzający feerią barw, ale w zupełnie innym stylu, znacznie bardziej dynamicznym. W przewodniku wyczytaliśmy, że nie dość, że praca była przez brazylijskiego artystę częściowo improwizowana, to jeszcze do jej powstania przyczynili się mieszkańcy osiedla. By przyspieszyć malowanie, chętni “kolorowali” fragmenty na samym dole.

### Gualicho – *Eternia* (Skarżyńskiego 10a) 
Ten szczególnie spodobał się Maciejowi, więc go z grubsza zacytuję: *nie wygląda jak mural, tylko jak taki wielki obraz. Ma ładne, wyraziste kolory, w przeciwieństwie do niektórych, i taka ładną kompozycję*

### Piotr Szwabe – *Memling. Sąd ostateczny żywy w pikselach* (Skarżyńskiego 6f)
Następny mural Piotra Szwabe wyjątkowo przypadł do gustu akurat mi, bo Maciej stwierdził, że nic tam nie widzi. To przetłumaczony na język street-artu fragment tryptyku Memlinga, który obecnie znajduje się w Muzeum Narodowym w Gdańsku. Moim zdaniem był to świetny sposób na nawiązanie, a ja w ogóle lubię, jak sztuka ze sobą rozmawia.

{% include post-carousel-folder.html folder="zaspa" images="6" alt="Murale na zaspie" ratio="4 / 3" %}

### Opiemme – *Wir i napromieniowanie tęczą* (Dywizjonu 303 9d)
To dzieło nie nasunęło nam kreatywnej interpretacji, poza tą zaproponowaną w przewodniku, ale i tak zrobiło na nas wrażenie. Może właśnie przez niejednoznaczność? Co ciekawe, włoski artysta wykorzystał cytat z wiersza Szymborskiej. Jak międzynarodowo!

{% include post-carousel-folder.html folder="zaspa" images="7" alt="Murale na zaspie" ratio="4 / 3" %}

### Rafał Roskowiński – *Tukany* (Bajana 3c)
Naprawde nie wiem, czemu za motyw wybrano akurat tukany, ale w pokrętny sposób i te egzotyczne ptaki, i tęczowe zawijasy idealnie wpasowały się w klimat Zaspy. Oczywiście naszym zdaniem.

### Klaus Klinger – *The New Future* (Bajana 5a)
Osiedle z lat 80. musiało podsunąć komuś pomysł nawiązania muralem do socrealizmu. Kobieta namalowana przez Klingera zamiast na traktor, idzie do supermarketu. Im dłużej przyglądaliśmy się tej scence, tym więcej w niej widzieliśmy, więc zdecydowanie udało jej się pobudzić nasze szare komórki.

### Praca zbiorowa – *Hołd malarstwu polskiemu* (Bajana 9a i 11c)
W tym przypadku najbardziej zaciekawiła nas nie treść, tylko forma. Mural składa się z dwóch części, namalowanych na ścianach szczytowych sąsiednich bloków. Pod odpowiednim widać, jak łączy się ten dyptyk. Jak wskazuje tytuł, twórcy inspirowali się twórczością polskich malarzy.

{% include post-carousel-folder.html folder="zaspa" images="8" alt="Murale na zaspie" ratio="3 / 4" %}

## Nasze wrażenia
Chociaż nie jesteśmy żadnymi znawcami street-artu, podczas naszych podróży lubimy odwiedzać miejsca przeobrażone w taki wyjątkowy sposób. Zdecydowanie nie żałujemy, że sporód wielu atrakcji w Gdańsku zdecydowaliśmy się właśnie na spacer po Zaspie.

## Informacje praktyczne
- [Na tej stronie](https://muralegdanskzaspa.pl/jak-zwiedzac/) znajdziecie przewodnik i mapę przygotowane przez Instytut Kultury Miejskiej
- Przejście Kolekcji Malarstwa Monumentalnego mniej więcej wyznaczoną trasą, oczywiście z krótkimi przystankami pod każdym muralem, zajęło nam prawie **2,5h** (łącznie ponad 6km)
- W lipcu i sierpniu murale na Zaspie można zobaczyć z lokalnym przewodnikiem. Szczegóły na tej samej stronie internetowej 
- Na Zaspę przyjechaliśmy samochodem. Parkowanie było pewnym wyzwaniem, ale na pewno zawsze trafi się jakieś wolne miejsce. Z centrum można również bez problemów dojechać komunikacją miejską
