---
title: "Co zabieramy w podróż życia?"
image: "/assets/images/pack/cover.jpg"
feature_text: "Co zabieramy w podróż życia?"
categories:
- categories.practical
- categories.articles
featured: false
---

**Przygotowując się do wyjazdu, przeczytaliśmy tonę praktycznych artykułów na temat pakowania się w daleką podróż, np. dookoła świata. Wbrew pozorom, każdy pomysł czymś się wyróżniał. Nasza lista rzeczy do zabrania uwzględnia specyfikę wyprawy - przemieszczanie się autostopem, częste spanie w namiocie, prowadzenie bloga z trasy, brak określonego terminu powrotu.**

<!-- more -->

W czasie kompletowania bagażu doszliśmy do wniosku, że od pewnego momentu długość planowanego wyjazdu przestaje mieć znaczenie. Bez względu na to, czy będziesz w trasie miesiąc czy rok, podstawowa lista pozostaje taka sama. Po prostu trzeba robić pranie minimum co 2 tygodnie. Wzięliśmy za to pod uwagę zmienność stref klimatycznych oraz pór roku. Czujemy się przygotowani na ponad 30°C w Rio de Janeiro i mróz na Ziemi Ognistej.

A teraz konkrety! Oto, co pakujemy.
## Plecaki
- damski plecak Deuter Aircontact Pro 55+15 SL
- męski plecak Deuter Aircontact Pro 60+15

Wybraliśmy je ze względu na wytrzymałość, idealną dla naszych potrzeb pojemność, ale przede wszystkim z powodu genialnego systemu nośnego. Modele zdecydowanie nie należą do lekkich (ważą odpowiednio 3080 i 3180 g), jednak rozkładają ciężar o niebo lepiej niż inne przymierzane przez nas plecaki.

{% include post-carousel.html images="assets/images/pack/14.jpg" alt="Plecaki na podróż" ratio="1" %}

## Odzież
### Dla niej
- kurtka przeciwdeszczowa Quechua MH100 - niby najtańsza z Decathlonu, a zaskakująco dobra
- kurtka puchowa Quechua Trek 100 - jw.
- polar
- 7 t-shirtów
- sukienka - bardziej na plażę niż na bal
- spódniczka
- 2 pary długich spodni trekkingowych
- krótkie spodnie
- koszula flanelowa – idealna, kiedy jest zbyt chłodno na koszulkę z krótkim rękawem, a zbyt ciepło na polar
- bielizna
- bielizna termiczna 
- chusta wielofunkcyjna typu buff – może być Twoją czapką, szalikiem, opaską albo kominiarką
- bandana – do noszenia na głowie w słoneczne dni
- czapka – dobra, to lekka ściema – zamierzam kupić sobie jakąś ładną czapkę jako pamiątkę z Ameryki Południowej
- rękawiczki
- strój kąpielowy
- okulary korekcyjne z nakładkami przeciwsłonecznymi

{% include post-carousel.html images="assets/images/pack/12.jpg" alt="Damska odzież na podróż" ratio="1" %}


### Dla niego
- kurtka przeciwdeszczowa Quechua MH100
- pulower Primaloft Montane (kupiony zbyt dawno, żebym pamiętał model - M.)
- polar
- 2 pary długich spodni trekkingowych – w tym jedne z odpinanymi nogawkami, do przerobienia na kolejne krótkie
- krótkie spodnie
- 7 t-shirtów
- bielizna
- bielizna termiczna
- chusta wielofunkcyjna typu buff
- bandana
- czapka
- rękawiczki
- kąpielówki
- okulary przeciwsłoneczne

{% include post-carousel.html images="assets/images/pack/13.jpg" alt="Męska odzież na podróż" ratio="1" %}


## Buty
### Dla niej
- niskie buty trekkingowe Salomon Ellipse 2 LTR
- sportowe sandały Teva Hurricane XLT

### Dla niego
- niskie buty trekkingowe La Sportiva Boulder X - Maciej używa ich już ponad 5 lat, więc nie zdziwimy się, jeżeli w trakcie podróży będzie potrzebna wymiana
- sportowe sandały Teva Hurricane XLT
Dodatkowo mamy klapki pod prysznic – jedną parę, żeby było lżej 😉

{% include post-carousel.html images="assets/images/pack/11.jpg" alt="Buty na podróż" ratio="1" %}


## Do spania
- namiot Karrimor Elite Ridge 2 – sprawdził się już na izraelskiej pustyni, zboczach Mount Blanc i klifach Islandii
- śpiwór puchowy Naturehike
- śpiwór syntetyczny McKinley
- 2 wkładki do śpiworów - lekkie i łatwe do wyprania
- 2 maty do spania

{% include post-carousel.html images="assets/images/pack/10.jpg" alt="Sprzęt do spania" ratio="1" %}


## Przenośna kuchnia
- palnik
- menażka – służy nam jako garnek, a razem z pokrywką zastępuje z powodzeniem zastawę stołową
- kubek – lekki, metalowy
- 2 sporki – dla niewtajemniczonych, łyżka i widelec w jednym
- nóż składany
- zapalniczka
- krzesiwo – malutkie, na czarną godzinę
- plastikowe pojemniczki na przyprawy – chyba nikt nie chciałby nosić w plecaku kilograma soli
- tabletki do odkażania wody


## Elektronika i okolice
- laptop Dell Chromebook 11 - z zainstalowanym Linuxem, kupiony na Allegro za okrągłe 160 zł 
- dwa telefony: Huawei P8 Lite i LG G2 Mini – oba mają swoje lata, ale powinny wystarczyć na nasze skromne potrzeby
- aparat Sony RX100 III – bardzo satysfakcjonująca proporcja jakości zdjęć do rozmiaru urządzenia
- czytnik e-booków Kindle 4
- powerbank – pojemność 10000 mAh
- ładowarka z poczwórnym wyjściem – do wygodnego zasilania wszystkiego, co wymieniono powyżej
- ładowarka samochodowa
- ładowarka solarna – dodana do naszej listy przed samym wyjazdem – dziękujemy za prezent!
- adapter do wtyczek – w samej Ameryce Południowej występują trzy zupełnie różne rodzaje gniazdek, więc trzeba być przygotowanym
- rozgałęźnik – choćbyśmy mieli 50 gniazdek do dyspozycji, adapter posiadamy tylko jeden
- karty pamięci 64 i 32G – zamierzamy przechowywać zdjęcia w chmurze, miejsce na kartach będziemy wykorzystywać z dala od internetu 
- słuchawki
- 2 latarki czołówki

{% include post-carousel.html images="assets/images/pack/9.jpg" alt="Sprzęt elektroniczny do wzięcia w podróż"  ratio="1" %}

## Kosmetyczka
### Dla niej
- ręczniki szybkoschnące – duży i mały
- szczoteczka do zębów
- żel pod prysznic
- szampon
- dezodorant
- szczotka do włosów
- kilka gumek do włosów i wsuwek
- maszynka do golenia
- penseta
- kubeczek menstruacyjny

### Dla niego
- ręcznik szybkoschnący
- szczoteczka do zębów
- płyn do mycia ciała i włosów
- dezodorant
- golarka elektryczna

### Wspólne
- pasta do zębów
- mocny krem z filtrem UV
- repelent na owady
- obcinacz do paznokci

## Dokumenty i pieniądze
- paszporty – nówki sztuki, jeszcze bez żadnych stempli
- dowody osobiste
- prawa jazdy
- karty Euro26
- kopie paszportów
- pokrowce wodoszczelne na dokumenty
- karty płatnicze – Revolut (Visa), Curve (MasterCard)
- portfele zakładane pod ubrania
- fałszywe portfele – w środku stare karty oraz trochę gotówki – patent na kieszonkowców

## Inne
- kijki trekkingowe - 2 zestawy
- zestaw małego krawca – nić, igła, agrafki
- zestaw małego majsterkowicza – scyzoryk, małe nożyczki, taśma izolacyjna, sznurek
- woreczki strunowe
- torby płócienne – na zakupy lub ubrania do prania
- rolka papieru toaletowego
- marker – niezbędny gadżet autostopowicza (kartony do kompletu będziemy oczywiście zdobywać na bieżąco)
- notes, długopisy i ołówek
- karty do gry
- mały statyw typu gorilla
- naklejki z logiem i adresem bloga
- apteczka – podstawowe leki, zestaw plastrów, bandaże

Brzmi jak tona rzeczy? Słusznie. Nasze plecaki chyba jeszcze nigdy nie były takie ciężkie, przede wszystkim ze względu na rekordową ilość elektroniki. Mamy jednak wystarczającą rezerwę, by zabrać zapas żywności na dłuższy pobyt poza cywilizacją.

Długo rozważaliśmy, co spakować w podróż, a co zostawić w kartonach na strychu u rodziców. Nie mamy wyposażenia na każdą okoliczność, ale nie mieliśmy takich aspiracji. Wychodzimy z prostego założenia, że na trasie naszej wyprawy będą sklepy, w których w razie potrzeby uzupełnimy ekwipunek.