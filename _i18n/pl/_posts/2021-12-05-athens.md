---
title: "Czym kupiły nas Ateny?"
image: "/assets/images/athens/12.jpg"
categories:
- categories.greece
- categories.practical
featured: false
---

**Ostatniego dnia pobytu w Atenach deklarowałam, że już nigdy w życiu tu nie przyjadę i wpisuję miasto na czarną listę. Była to mało rozsądna pogróżka, bo prawdę powiedziawszy, oprócz kilkunastu godzin poprzedzających powrót do Polski, naprawdę nam się podobało.**

<!-- more -->

Na początku należy się wyjaśnienie. Grecka stolica zrobiła na nas złe ostatnie wrażenie, bo dosłownie przed wyruszeniem na lotnisko skradziono nam plecak z telefonem i portfelem w środku. Nie mieliśmy pod ręką faktycznego złodzieja, więc pomstowaliśmy na całe miasto.

Jeżeli chodzi o pierwsze wrażenie (i praktycznie każde kolejne), Ateny zdecydowanie przerosły nasze oczekiwania. Co nas ujęło w tym mieście?

## Łatwo zapomnieć, że to ciągle Europa
Mając już porównanie z innymi kontynentami, zwykle w dużych miastach Unii Europejskiej czujemy się jak w domu. Czy to Paryż, Sztokholm czy Porto, galerie handlowe, supermarkety czy nawet ruch uliczny funkcjonują właściwie tak samo. Ateny, jak na jedną z kolebek europejskiej cywilizacji, zupełnie nie wpisały się w zakodowane w naszych głowach schematy europejskości. Zaskoczyły nas wszechobecne stragany pachnące przyprawami/rybą/oliwkami, chaotyczny układ ulic, miniaturowe, ultra wyspecjalizowane sklepiki, które w życiu nie utrzymałyby się w takiej choćby Warszawie… Krótko mówiąc, w Atenach czuliśmy się trochę jak w Istambule czy Jerozolimie, ale na pewno nie jak w Unii Europejskiej.

{% include post-carousel-folder.html folder="athens" images="2,3,4" alt="Zwiedzanie Aten" ratio="4 / 3" %}

## Aura wielkiej historii
Chociaż z czasów Sokratesa i Fidiasza pozostały głównie ruiny, w Atenach i tak łatwo zatopić się w marzeniach o podróży w czasie, do epoki rozkwitu demokracji, teatru oraz filozofii. Namacalność całej tej historii potęguje górujący nad miastem Akropol. Do tego szczątki dawnych świątyń i budowli można znaleźć nawet w najbardziej niespodziewanych zakamarkach, a nie tylko w miejscach z pierwszych stron przewodników, typu na Agorze Greckiej.

{% include post-carousel-folder.html folder="athens" images="9,10,5,8" alt="Zwiedzanie Aten" ratio="4 / 3" %}

## Niekończące się dachy miasta
Każdego dnia pobytu w Atenach staraliśmy się wejść na przynajmniej jedno wzgórze, by spojrzeć na miasto z lotu ptaka. Morze piaskowych murów i płaskich dachów to obraz, który zapamiętam z tej podróży chyba najdłużej. Z góry wydaje się, jakby Ateny idealnie zajęły całą dostępną przestrzeń, bo zabudowania z jednej strony kończą się dopiero na zboczach gór, a z drugiej odcina je nierówną kreską linia wybrzeża. Być może przez to, że ten punkt widokowy odwiedziliśmy jako pierwszy, szczególnie polecamy panoramę ze wzgórza Filopappou.

{% include post-carousel-folder.html folder="athens" images="6" alt="Zwiedzanie Aten" ratio="3 / 4" %}

## Zaułki rządzone przez koty
Stolica Grecji może nie przypaść do gustu przesądnym osobom, bo szansa, że czarny kot przetnie ci drogę, jest naprawdę spora. Koty wszelkiej maści spacerują ulicami, wylegują się w cieniu drzew albo obserwują życie miasta z najróżniejszych murków. Widzieliśmy kilka czworonogów łaszących się do turystów, ale większość zdawała się być ponad to. Moim zdaniem wszystkie te koty idealnie wpisują się w klimat miasta!

{% include post-carousel-folder.html folder="athens" images="7" alt="Zwiedzanie Aten" ratio="3 / 4" %}

## Jedzenie pity na każdy posiłek
W Atenach ani razu nie zdążyliśmy zgłodnieć, bo gdzie nie poszliśmy, trafialiśmy na knajpę z pitami. Jedliśmy i te klasyczne z gyrosem, i rozmaite wegetariańskie. Pewnie rozsądny człowiek wykorzystałby czas lepiej i spróbował znacznie więcej spośród potraw, które ma do zaoferowania kuchnia grecka, ale my serio zakochaliśmy się w picie. Zapiekankę pastitsio, sałatkę horiatiki i faszerowane warzywa zamówiliśmy z czystej przyzwoitości.

Zwiedzaliśmy Ateny przez cztery umiarkowanie intensywne dni. Pomijając epizod z kradzieżą (swoją drogą, wszystko przez zamówienie o jedną pitę za dużo, bo plecak straciliśmy w knajpie), zdecydowanie było warto tu przyjechać. Jeśli jeszcze kiedyś pojawi się okazja, chętnie wpadniemy jeszcze raz, bo serio, stolica Grecji po prostu ma w sobie coś niezwykłego.

{% include post-carousel-folder.html folder="athens" images="13" alt="Zwiedzanie Aten" ratio="1 / 1" %}