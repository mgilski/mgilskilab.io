---
title: "Dźwięki Wiednia w Domu Muzyki"
image: "/assets/images/house-of-music/1.jpg"
categories:
- categories.austria
- categories.practical
featured: false
---


**W stolicy Austrii zdecydowanie nie brakuje muzeów, poświęconych czy to sztukom plastycznym, czy historii. Podczas ostatniej wizyty odwiedziliśmy jedno z nich, nieco mniej znane, za to naprawdę wyjątkowe. Dom Muzyki! Zapraszamy na wspólną wycieczkę do miejsca, w którym dźwięki się materializują, a wielkim kompozytorom można zaglądać przez ramię.**

<!-- more -->

## Muzyczna twarz Wiednia
Że Wiedeń żyje muzyką, wie chyba każdy turysta. Obejrzenie spektaklu w tutejszej operze to marzenie melomanów z całego świata, bilety na Koncert Noworoczny w wykonaniu Filharmoników Wiedeńskich trzeba rezerwować z rocznym wyprzedzeniem, a rozmaite festiwale odbywają się właściwie przez cały rok. Do tego trzeba pamiętać, że swego czasu tworzyli tu tak znamienici kompozytorzy, jak Mozart, Beethoven, Haydn, Strauss czy Schubert. Biorąc pod uwagę wszystkie te akcenty, na muzeum dźwięku Wiedeń nadaje się lepiej, niż jakiekolwiek miasto w Europie.

## Za progiem wiedeńskiego Domu Muzyki
Haus der Musik, czyli właśnie Dom Muzyki, to przede wszystkim muzeum z pomysłem. Jego twórcy pokazali, że potrafią wyśmienicie kreować nastrój i utrzymywać uwagę gości, za pomocą różnorodności wystaw, gry światła i oczywiście samego dźwięku. Wystawy rozłożone są na czterech piętrach. Pierwsze z nich poświęcono historii **Filharmoników Wiedeńskich**, uważanych za jedną z czołowych orkiestr świata. Tutaj warto wspomnieć, że muzeum mieści się w budynku, w którym mieszkał Otto Nicolai, pierwszy dyrygent Filharmoników. 

Wchodząc na drugie piętro, notabene po grających schodach, przenosimy się do świata **akustyki**. Jak powstaje dźwięk? Co czyni z niego muzykę? Od czego zależy, ile słyszymy? Na te właśnie pytania odpowiada wystawa SONOTOPIA.

Ostatnią część Domu Muzyki zadedykowano **najsłynniejszym kompozytorom** związanym z Wiedniem. Zainteresowani mogą dokładnie obejrzeć oryginalne pamiątki czy też zagłębić się w ich życiorysy, ale osoby, które nie przepadają za takimi lekcjami historii, też nie będą się zawiedzione. Na pierwszym planie są tu ciekawostki i oczywiście muzyka.

{% include post-carousel-folder.html folder="house-of-music" images="3" alt="House of music in Vien" ratio="3 / 4" %}

## Czym oczarował nas Dom Muzyki?
Akurat żadne z nas nie ma większego problemu, kiedy w muzeum głównie się ogląda i czyta. Nie da się jednak ukryć, że im większa interaktywność, tym lepiej zapamiętujemy zwiedzanie. W tym zakresie Dom Muzyki w Wiedniu stanął na wysokości zadania. Skomponowaliśmy walca za pomocą rzutów kostką. Przekonaliśmy się, jak te same dźwięki postrzega pies, a jak płód w łonie mamy. Na własnych uszach odczuliśmy proces utraty słuchu przez Beethovena. Na koniec wreszcie mogliśmy sprawdzić swoje siły dyrygując wirtualną orkiestrą, która ruchy batuty przekłada (z mniejszym lub większym sukcesem) na melodię takich utworów jak *Eine kleine Nachtmusik* czy *Nad pięknym modrym Dunajem*.

Dzięki wizycie w muzeum dźwięku Wiedeń jeszcze bardziej będzie nam się kojarzyć z muzyką. Selfie z pomnikiem Mozarta albo Złotym Straussem to jedno. Przejście podróży od fali dźwiękowej do kompozycji jednego z wiedeńskich mistrzów, to już zupełnie inny poziom doświadczenia.

{% include post-carousel-folder.html folder="house-of-music" images="4" alt="House of music in Vien" ratio="3 / 4" %}

## Informacje praktyczne
- Haus der Musik znajduje się w ścisłym centrum Wiednia przy **Seilerstätte**, kilkaset metrów od opery i katedry
- Normalny bilet do muzeum kosztuje **€16**. Tańszy wstęp mają dzieci (**€7**), studenci i seniorzy (**€12**), do tego wchodząc po godzinie 20.00 można skorzystać z promocyjnej ceny **€8** – pewnym minusem jest wtedy czas na zwiedzanie ograniczony do dwóch godzin, czyli do zamknięcia
- Na zwiedzanie warto zarezerwować sobie **2-2,5 godziny**. To czas, który spokojnie wystarczy na obejrzenie wszystkich wystaw i skorzystanie z interaktywnych stanowisk, o ile nie trafi się na kolejki

{% include post-carousel-folder.html folder="house-of-music" images="2" alt="House of music in Vien" ratio="1 / 1" %}

