---
title: "Jak odwiedziliśmy 41 krajów, czyli nasze podróże 2014-2019"
image: "/assets/images/prev/cover.jpg"
feature_text: "Opis naszych poprzednich podróży"
categories:
- categories.thoughts
- categories.articles
featured: false
---


**Wystartowaliśmy z blogiem, żeby opowiadać o aktualnej wyprawie autostopem przez świat. Nie znaczy to oczywiście, że o wcześniejszych podróżniczych doświadczeniach nie mamy nic do powiedzenia! W oczekiwaniu na nowe historie, zapraszamy na małe podsumowanie naszych dotychczasowych zagranicznych wojaży.**

<!-- more -->

## 2014
### Hiszpania
Do dziś trudno powiedzieć, jakim cudem ten wyjazd doszedł do skutku. Byliśmy parą dopiero od dwóch miesięcy, Wera była niepełnoletnia, a do tego nie mieliśmy żadnego doświadczenia w jeżdżeniu autostopem poza Polską. Bez smartfonów, za to z porządnym atlasem drogowym Europy, dojechaliśmy z Poznania na wybrzeże Costa Blanca, do Altei. 

**Najfajniejsze doświadczenie**: jedzenie paelli na plaży, prosto z ogniska

**Najdziwniejsze doświadczenie**: nocleg w parku w centrum Walencji, przerwany przez uruchomione nad ranem spryskiwacze

{% capture alt %}{% t alt.spain %}{% endcapture %}
{% include post-carousel.html images="assets/images/prev/74.jpg" alt=alt ratio="3 / 4" %}

## 2015
### Amsterdam – Bruksela – Luksemburg
Korzystając z rozpoczynających się wakacji, odwiedziliśmy stolice Beneluksu. Eksperymentalnie zabraliśmy ze sobą gitarę i mimo co najwyżej średnich zdolności muzycznych, zarobiliśmy całkiem imponującą kwotę grając na ulicach. Po ok. 4h mieliśmy wystarczający budżet na trwającą cały miesiąc podróż do Gruzji i Armenii.

**Najfajniejsze doświadczenie**: wieczorny spacer przez luksemburski Parc des Trois Glands z latającymi wokół nas świetlikami


**Najdziwniejsze doświadczenie**: jazda autostopem z Holendrem palącym lokalne dobra za kierownicą

{% capture alt %}{% t alt.amsterdam %}{% endcapture %}
{% include post-carousel.html images="assets/images/prev/8.jpg, assets/images/prev/7.jpg, assets/images/prev/cover.jpg" alt=alt ratio="2 / 3" %}

### Gruzja i Armenia
Plan wyprawy zakładał dotarcie w Kaukaz okrężną drogą przez Bałkany i Turcję. Po drodze zachwyciła nas malownicza Zatoka Kotorska w Czarnogórze, albańskie krajobrazy, a także zwariowane targowiska Stambułu. Pierwszy raz nasz zestaw języków obcych (angielski, hiszpański i francuski) okazał się niewystarczający, więc przez miesiąc komunikowaliśmy się przeplatając polski z pojedynczymi słowami po rosyjsku. Oraz przez Google Translate.

**Najfajniejsze doświadczenie**: spotkanie z obezwładniającą gościnnością Ormian

**Najdziwniejsze doświadczenie**: nocna ulewa w Gruzji, podczas której nasz namiot został niemal porwany przez wzburzoną rzekę

{% capture alt %}{% t alt.georgia %}{% endcapture %}

{% include post-carousel.html images="assets/images/prev/3.jpg, assets/images/prev/2.jpg, assets/images/prev/1.jpg, assets/images/prev/4.jpg, assets/images/prev/5.jpg" alt=alt ratio="2 / 3" %}

## 2016
### Dookoła Bałtyku
Nazwa myląca, bo nie tylko okrążyliśmy Bałtyk, ale też dotarliśmy nad Morze Barentsa, na północny skraj Europy. Po raz pierwszy przekroczyliśmy magiczną linię koła podbiegunowego! Zwiedziliśmy kilka naprawdę niesamowitych miast – Wilno, Rygę, Tallinn, Helsinki i Sztokholm. Norweskie krajobrazy, z fiordami, górami i stadami reniferów na drogach, po prostu rzuciły nas na kolana. 

**Najfajniejsze doświadczenie**: trekking po Parku Narodowym Oulanka w Laponii

**Najdziwniejsze doświadczenie**: w Norwegii co drugi podwożący nas kierowca był Polakiem, a jeden z nich dodatkowo katolickim księdzem (jedynym w promieniu 600 km)

{% capture alt %}{% t alt.baltic %}{% endcapture %}
{% include post-carousel.html images="assets/images/prev/15.jpg, assets/images/prev/10.jpg, assets/images/prev/11.jpg, assets/images/prev/12.jpg, assets/images/prev/13.jpg, assets/images/prev/14.jpg, assets/images/prev/9.jpg, assets/images/prev/16.jpg, assets/images/prev/17.jpg" alt=alt ratio="2 / 3" %}

### Anglia i Walia
Wielka Brytania powitała nas piękną pogodą, która trwała przez całą wizytę. Szach-mat, statystyko! W Anglii odwiedziliśmy Bristol, Bath i wąwóz Cheddar (tak, właśnie stąd pochodzi ser cheddar), a w Walii Cardiff i Aberystwyth.

**Najfajniejsze doświadczenie**: spacery wzdłuż wybrzeża i jedzenie fish and chips w Aberystwyth

**Najdziwniejsze doświadczenie**: pierwsze w życiu spotkanie z lewostronnym ruchem

{% capture alt %}{% t alt.england %}{% endcapture %}
{% include post-carousel.html images="assets/images/prev/67.jpg, assets/images/prev/68.jpg, assets/images/prev/69.jpg, assets/images/prev/71.jpg, assets/images/prev/72.jpg, assets/images/prev/73.jpg" alt=alt ratio="2 / 3" %}

### Izrael i Palestyna
Wyjazd został zaplanowany w kwadrans. Tyle zajęło wstanie z łóżka, porozmawianie przez telefon i kupienie okazyjnych biletów. Wraz z przyjaciółmi spędziliśmy w Izraelu 12 dni, podczas których przemierzyliśmy zaułki Jerozolimy, przekroczyliśmy mroczny mur, by dotrzeć do Betlejem, wspięliśmy się do ruin Masady i wędrowaliśmy po pustyni Negew.

**Najfajniejsze doświadczenie**: przebywanie w otoczeniu miejsc wyjętych wprost z kart Biblii, co przez cały pobyt wydawało nam się trochę nierealne

**Najdziwniejsze doświadczenie**: kąpiel w Morzu Martwym

{% capture alt %}{% t alt.izrael %}{% endcapture %}
{% include post-carousel.html images="assets/images/prev/19.jpg, assets/images/prev/18.jpg, assets/images/prev/20.jpg, assets/images/prev/21.jpg, assets/images/prev/22.jpg, assets/images/prev/23.jpg, assets/images/prev/24.jpg, assets/images/prev/25.jpg, assets/images/prev/26.jpg" alt=alt ratio="2 / 3" %}

## 2017
### Peru
Zdecydowanie najdalsza z naszych dotychczasowych wypraw. Zwiedzaliśmy kraj Inków przez dwa tygodnie, a dzięki długim przesiadkom zobaczyliśmy również stolicę Meksyku. Każdy dzień w cieniu andyjskich szczytów wspominamy wspaniale. Czy może być inaczej, skoro weszliśmy na 5-tysięcznik, płynęliśmy łodzią po jeziorze Titicaca i pogłaskaliśmy alpakę? Wróciliśmy do Europy oczarowani Ameryką Południową, z mocnym postanowieniem ponownej, najlepiej znacznie dłuższej wizyty.

**Najfajniejsze doświadczenie**: trekking dnem kanionu Cotahuasi, gdzie spotkaliśmy kilka osłów, za to żadnych turystów

**Najdziwniejsze doświadczenie**: jazda autostopem na przyczepie w towarzystwie dwóch koni

{% capture alt %}{% t alt.peru %}{% endcapture %}
{% include post-carousel.html images="assets/images/prev/30.jpg, assets/images/prev/28.jpg, assets/images/prev/29.jpg, assets/images/prev/27.jpg, assets/images/prev/31.jpg, assets/images/prev/32.jpg, assets/images/prev/44.jpg" alt=alt ratio="2 / 3" %}

### Islandia
Większość budżetu na podróże 2017 wykorzystaliśmy na lot do Peru, dlatego owszem, wybraliśmy się na Islandię, ale z samym bagażem podróżnym i praktycznie bez pieniędzy. O dziwo, jakoś udało nam się przetrwać w tym horrendalnie drogim kraju – głównie dzięki namiotowi oraz liofilizowanym posiłkom. Warto było znosić wszelkie niedogodności dla zorzy polarnej!

**Najfajniejsze doświadczenie**: gapienie się przez dobre pół dnia na lód dryfujący po lagunie Jökulsárlón

**Najdziwniejsze doświadczenie**: noc w namiocie, który niemal kładł się na ziemi z powodu wichury

{% capture alt %}{% t alt.iceland %}{% endcapture %}
{% include post-carousel.html images="assets/images/prev/36.jpg, assets/images/prev/34.jpg, assets/images/prev/35.jpg, assets/images/prev/33.jpg, assets/images/prev/37.jpg, assets/images/prev/38.jpg, assets/images/prev/39.jpg, assets/images/prev/40.jpg, assets/images/prev/41.jpg, assets/images/prev/42.jpg, assets/images/prev/43.jpg" alt=alt ratio="2 / 3" %}

## 2018
### Węgry – Serbia – Słowenia – Bawaria
Rok 2018 zdominował projekt ŚLUB, więc na pierwszy dłuższy wyjazd wybraliśmy się dopiero w pseudo miesiącu miodowym. Dlaczego pseudo? Nie była to podróż poślubna z prawdziwego zdarzenia, tylko przeprowadzka do Francji wyjątkowo okrężną drogą. Nasza trasa wiodła m.in. przez Budapeszt, Belgrad, jezioro Bled i Alpy Salzburskie.

**Najfajniejsze doświadczenie**: wieczorny spacer po Budapeszcie

**Najdziwniejsze doświadczenie**: wejście po ciemku w sam środek stada krów w Parku Narodowym Berchtesgaden (tak to jest, kiedy człowiek zapomni latarki…)



### Paryż
Ze względu na studia Macieja, pierwsze 3 miesiące małżeństwa spędziliśmy na przedmieściach Paryża. Życie we Francji nie przypadło nam do gustu, o czym wiedzą doskonale wszyscy, którzy musieli wysłuchiwać naszych narzekań (przepraszamy). Samo miasto jednak polubiliśmy. Polecamy wybrać się tam poza sezonem, tak jak my, bo tłumy turystów na pewno skutecznie psują klimat.

**Najfajniejsze doświadczenie**: zwiedzanie Luwru od otwarcia do zamknięcia

**Najdziwniejsze doświadczenie**: zmagania z francuską biurokracją

{% capture alt %}{% t alt.paris %}{% endcapture %}
{% include post-carousel.html images="assets/images/prev/64.jpg, assets/images/prev/63.jpg, assets/images/prev/65.jpg" alt=alt ratio="2 / 3" %}

## 2019
### Północne Niemcy
Wraz z parą przyjaciół obraliśmy sobie za cel majówkowej wycieczki odwiedzenie Hamburga, a konkretnie Miniatur Wunderland, czyli największego modelu kolejki na świecie. Pewnie nie dla każdego brzmi to zachęcająco, ale cała nasza czwórka była zachwycona. Po drodze wpadliśmy do kilku hanzeatyckich miast i na wyspę Rugię.

**Najfajniejsze doświadczenie**: wizyta w Miniatur Wunderland

**Najdziwniejsze doświadczenie**: jedzenie hamburgerów w Hamburgu (może nie jest to dziwne, ale za to zabawne, więc nie mogłam się powstrzymać – W.)

{% capture alt %}{% t alt.germany %}{% endcapture %}
{% include post-carousel.html images="assets/images/prev/45.jpg, assets/images/prev/46.jpg, assets/images/prev/47.jpg, assets/images/prev/48.jpg, assets/images/prev/49.jpg, assets/images/prev/50.jpg" alt=alt ratio="2 / 3" %}

### Irlandia
We wrześniu chcieliśmy wybrać się do jakiegokolwiek kraju, w którym jeszcze nas nie było. Wyszukiwarka tanich lotów wytypowała Irlandię. Stereotyp potwierdzony – już z samolotu dostrzegliśmy, że trawa jest tam bardziej zielona. Spędziliśmy cudowny tydzień w otoczeniu klifów, ruin zamków, kolorowych owiec i przesympatycznych, choć mówiących z dziwnym akcentem, Irlandczyków.

**Najfajniejsze doświadczenie**: podziwianie krajobrazów półwyspu Dingle

**Najdziwniejsze doświadczenie**: jazda samochodem ze starszym panem, którego córka musiała na bieżąco tłumaczyć z angielskiego na angielski

{% capture alt %}{% t alt.ireland %}{% endcapture %}
{% include post-carousel.html images="assets/images/prev/54.jpg, assets/images/prev/52.jpg, assets/images/prev/53.jpg, assets/images/prev/51.jpg, assets/images/prev/55.jpg, assets/images/prev/56.jpg, assets/images/prev/57.jpg, assets/images/prev/58.jpg, assets/images/prev/59.jpg, assets/images/prev/60.jpg, assets/images/prev/61.jpg, assets/images/prev/62.jpg" alt=alt ratio="2 / 3" %}

Powyżej napisaliśmy tylko o dłuższych wyjazdach zagranicznych. Moglibyśmy do tego dodać wypady w różne zakątki (najlepiej górskie) Polski oraz okolicznych państw. Nie chodzi o to, że uważamy je za mało ciekawe – po prostu trudno wszystko zliczyć i zmieścić w jednym wpisie… Nie uwzględniliśmy również indywidualnych podróży, takich jak wyjazdy Wery do Rzymu czy samotna wędrówka Macieja po Gorganach.

Jeśli kiedyś wystarczy nam czasu i weny, zamierzamy spisać przygody z ostatnich pięciu lat w bardziej zaawansowanej formie.
