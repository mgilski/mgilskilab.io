---
title: "W betonowej dżungli São Paulo"
image: "/assets/images/sp/cover.jpg"
feature_text: "W betonowej dżungli São Paulo"
categories:
- categories.brazil
featured: false
---

**Największe miasto Brazylii i całej Ameryki Południowej. Ponad 20 milionów mieszkańców – brzmi przytłaczająco. Określenie “betonowa dżungla” idealnie pasuje do São Paulo. Gubienie się w tym gąszczu podobało nam się znacznie bardziej, niż spodziewaliśmy się na wstępie.**

<!-- more -->

## Nasza droga do SP

Pierwszy raz jesteśmy w podróży i wcale nam się nie spieszy. W trasie z Rio de Janeiro do São Paulo wręcz nadużywaliśmy tego luksusu. Już na przedmieściach przyjęliśmy **zaproszenie kierowcy do domku letniskowego na wybrzeżu**, żebyśmy poznali jego rodzinę. Konkretnie jej pozostałą część, bo główną orędowniczką zabrania nas była zajmująca przednie siedzenie 11-letnia Mayra.

Spędziliśmy popołudnie w iście brazylijskim stylu, spacerując po plaży i relaksując się w cieniu. Komunikowaliśmy się z nowymi znajomymi głównie przez **Google Translate**, ale rozmowa toczyła się zaskakująco płynnie. W nocy wspólnie wybraliśmy się na **lokalną imprezę karnawałową**.

{% include post-carousel.html images="assets/images/sp/01.jpg, assets/images/sp/02.jpg, assets/images/sp/03.jpg" alt="Costa Verde" ratio="2 / 3" %}

Ruszyliśmy w dalszą drogę nakarmieni lodami zrobionymi z rosnących na podwórku kokosów. Podobno Mayra, która zdążyła naprawdę przywiązać się do Wery, już marzy, żeby odwiedzić nas w Polsce (najlepiej zimą, żeby zobaczyć śnieg).

Następnego dnia zwiedziliśmy prawdziwą perełkę tej części Brazylii – kolonialne miasteczko **Paraty**, wpisane na listę światowego dziedzictwa **UNESCO**. W czasach gorączki złota był to arcyważny port, z którego wyruszały do Portugalii statki pełne cennego kruszcu. Po bogatej historii pozostało świetnie zachowane centrum z bielonymi ściany i brukiem na uliczkach.

{% include post-carousel.html images="assets/images/sp/06.jpg, assets/images/sp/35.jpg, assets/images/sp/05.jpg" alt="Miasto Paraty" ratio="2 / 3" %}


## Miasto w trzy dni
Z Paraty do São Paulo dojechaliśmy z niemiecko-argentyńską parą, która akurat spieszyła się na samolot do Buenos Aires. Poszło to szybciej, niż sobie wyobrażaliśmy, więc spędziliśmy noc w namiocie, a dopiero kolejnego dnia przenieśliśmy się do naszego hosta Michaela. Było to pierwszy nocleg podczas tej podróży umówiony przez portal **Couchsurfing**. Bardzo pozytywne doświadczenie!

Nie zwiedzaliśmy jakoś przesadnie intensywnie, ale udało nam się dotrzeć do kilku ciekawych miejsc wSão Paulo. W porównaniu z Rio de Janeiro, miasto wydało nam się znacznie bardziej “poukładane”. Niestety, i tu widać było ciemniejszą stronę w postaci licznych bezdomnych na ulicach. W panoramie brakowało za to faweli, ale to tylko dlatego, że rozciągają się na obrzeżach metropolii.

{% include post-carousel.html images="assets/images/sp/15.jpg" alt="Wieżowce w centrum Sao Paulo" ratio="2 / 3" %}
## Co zobaczyliśmy w São Paulo?
### Katedra
Świątynia zdecydowanie imponuje rozmiarem. Z pozoru to typowy kościół neogotycki, ale przyglądając się bliżej licznym ornamentem, dostrzegamy sporo charakterystycznych nawiązań do brazylijskiej natury. Co ciekawe, katedrę zbudowano dokładnie na linii zwrotnika koziorożca.

{% include post-carousel.html images="assets/images/sp/13.jpg" alt="Katedra w Sao Paulo" ratio="3 / 2" %}

### Avenida Paulista
Biznesowa arteria otoczona z obu stron szklanymi wieżowcami. Zdecydowanie widać tutaj, dlaczego to miasto często porównuje się do Nowego Jorku… Przy Avenida Paulista wznosi się gmach słynnego muzeum MASP. My zrezygnowaliśmy, ze względu na ograniczony czas na zwiedzanie São Paulo, ale podobno warto.

{% include post-carousel.html images="assets/images/sp/18.jpg" alt="Wieżowce przy Avenida Paulista" ratio="3 / 2" %}

## Liberdade: Dzielnica Japońska
Nie byliśmy ani w Japonii, ani nawet w żadnym innym Japantown, ale w tej części São zdecydowanie wydało nam się orientalnie. Niemal każdy lokal zajmują tutaj tematyczne sklepy i restauracje, a nad ulicami wiszą lampiony.

{% include post-carousel.html images="assets/images/sp/14.jpg" alt="Dzielnica japońska Sao Paulo" ratio="3 / 2" %}

### São Bento
Bogato zdobiony kościół należący do **benedyktynów**. Nie tylko go zwiedziliśmy, ale również uczestniczyliśmy w niedzielnej Mszy Świętej, którą uświetnił **chorał gregoriański**. Trwające 1,5h nabożeństwo po portugalsku polecamy jednak tylko zdeterminowanym.

### Park Ibirapuera
Brazylijski odpowiednik Central Parku. Ogromna powierzchnia 221 hektarów daje świetną przestrzeń, by uciec od miejskich hałasów. Ibirapuerę warto odwiedzić również ze względu na fantazyjne budowle autorstwa **Oscara Niemeyera** oraz skryte w nich muzea. Nam udało się zwiedzić wystawę w Muzeum Sztuki Nowoczesnej. Planowaliśmy również wstąpić do Muzeum Afrobrazylijskiego, ale niestety okazało się zamknięte na czas karnawału.

{% include post-carousel.html images="assets/images/sp/24.jpg" alt="Park Ibirapuera Sao Paulo" ratio="2 / 3" %}

### Vila Madalena
São Paulo to mekka dla miłośników **street-artu**, a jego największe zagęszczenie można znaleźć w dzielnicy Vila Madalena. To tutaj znajduje się uliczka **Beco do Batman**, której każdy skrawek pokrywa graffiti.

{% include post-carousel.html images="assets/images/sp/27.jpg, assets/images/sp/31.jpg" alt="Beco do Batman" ratio="2 / 3" %}

Podczas zwiedzania São Paulo zahaczyliśmy również o kilka innych wizytówek miasta, takich jak otoczona pięknym parkiem **galeria Pinakoteka**, monumentalny **Teatro Municipal** i zaprojektowany przez Niemeyera (to dla nas wystarczający argument, żeby gdzieś podejść) wieżowiec **Copan**. 

Nasz werdykt jest taki, że miasto zdecydowanie nie należy do tych klasycznie ładnych, niemniej jednak, na swój sposób fascynuje. Na pewno nie przypominało żadnego miejsca, które widzieliśmy do tej pory.

## Informacje praktyczne
- Metro w São Paulo jest nowoczesne, wygodne i czyste. Przejazd kosztuje **4,4 BRL**, czyli relatywnie mało, jak na to, że można w ten sposób przejechać miasto wzdłuż i wszerz
- Jeszcze kilka lat temu miejski karnawał praktycznie nie istniał, ale obecnie należy do największych w Brazylii. Parada szkół samby nie jest tu aż tak spektakularna jak w Rio, za to uliczne imprezy mają co najmniej taki sam rozmach. Warto pamiętać, że z tego powodu ulice, przystanki autobusowe i stacje metra bywają zamknięte
- Polecono nam otwartą 24h/dobę knajpę **Estadão** przy Viaduto Nove de Julho, czyli w ścisłym centrum. Bez wahania przekazujemy to polecenie dalej. Pyszna kuchnia brazylijska, a szczególnie kanapki.

{% include post-gallery.html images="assets/images/sp/01.jpg, assets/images/sp/02.jpg, assets/images/sp/03.jpg, assets/images/sp/04.jpg, assets/images/sp/05.jpg, assets/images/sp/06.jpg, assets/images/sp/35.jpg, assets/images/sp/36.jpg, assets/images/sp/07.jpg, assets/images/sp/08.jpg, assets/images/sp/09.jpg, assets/images/sp/10.jpg, assets/images/sp/11.jpg, assets/images/sp/13.jpg, assets/images/sp/14.jpg, assets/images/sp/15.jpg, assets/images/sp/17.jpg, assets/images/sp/18.jpg, assets/images/sp/19.jpg, assets/images/sp/20.jpg, assets/images/sp/21.jpg, assets/images/sp/22.jpg, assets/images/sp/23.jpg, assets/images/sp/24.jpg, assets/images/sp/26.jpg, assets/images/sp/27.jpg, assets/images/sp/28.jpg, assets/images/sp/29.jpg, assets/images/sp/cover.jpg, assets/images/sp/31.jpg, assets/images/sp/32.jpg, assets/images/sp/33.jpg" collage1="assets/images/sp/01.jpg" collage2="assets/images/sp/28.jpg" collage3="assets/images/sp/17.jpg" %}