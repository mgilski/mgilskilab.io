---
title: "275 kaskad Iguazu"
image: "/assets/images/iguazu/cover.jpg"
feature_text: "275 kaskad Iguazu"
categories:
- categories.argentina
featured: false
---

**Lubicie wodospady? My bardzo! Park Narodowy Iguazu podnosi jednak to pojęcie do zupełnie nowego poziomu. Dzięki różnorodności wytyczonych tras, można oglądać ten cud natury z kilku perspektyw, więc o dziwo, gapienie się na wodę staje się atrakcją na cały dzień.**

<!-- more -->

## Największy, czy jeden z największych?

Odpowiedź do teraz nie jest dla nas oczywista. Do niedawna mieliśmy Iguazu za **największy wodospad świata** (tak twierdzi artykuł na Wikipedii na jego temat), ale po dłuższym przeglądaniu internetów okazało się, że nie wygrywa ani pod względem szerokości, ani przepływu wody. Nie ulega wątpliwości, że w pierwszej z tych kategorii wyprzedza 2-krotnie tak Niagarę, jak i Wodospady Wiktorii.

*Jeżeli ktoś z Was potrafi wytłumaczyć, z czego wynika dość powszechnie przyznawany status największego na świecie, zachęcamy do komentowania wpisu!*

Warto zwrócić uwagę, że rzeka Iguazu podczas swojej spektakularnej zmiany poziomów, dzieli się na 275 kaskad, łącznie szerokich na 2,7 km, wysokich na 60-82 m. Stąd prawdopodobnie niezbyt wyszukana, ale za to trafna nazwa w języku lokalnych Indian Guarani *Yguasu*, czyli Wielka Woda.

## Argentyna vs. Brazylia

Wodospady Iguazu leżą na granicy pomiędzy tymi dwoma państwami (a przy okazji rzut beretem od Paragwaju). 80% powierzchni znajduje się w Argentynie, 20% w Brazylii. Obie strony chronią parki narodowe wpisane na **listę światowego dziedzictwa UNESCO**.

Turyści z ograniczonym czasem lub budżetem (ten drugi przypadek to my) zazwyczaj decydują się odwiedzić tylko część argentyńską. Może ona się pochwalić bardziej rozbudowaną siatką tras, a do tego da się podejść bliżej wodospadów. Wybraliśmy ten wariant bez wahania i po dniu w parku narodowym nie mieliśmy poczucia niedosytu ani potrzeby gnania na stronę brazylijską.

## Co tu ładnego widać?

Park Narodowy Iguazu chroni nie tylko obszar wodospadów, ale również rozległy, bogaty we florę i faunę las deszczowy. W efekcie sceneria jest naprawdę bajkowa! Nie psują tego nawet turyści, ku naszemu pozytywnemu zaskoczeniu, wcale nie wszechobecni. Zdarzyło nam się nawet przejść kilkaset metrów i nie spotkać nikogo. Poprawka: żadnego człowieka, bo zwierzęta owszem.

Nasz spacer po parku przebiegał następująco…

### Zielony szlak (*Sendero Verde*, 600 m)

Po zakupie biletów niemal bez kolejki (wchodziliśmy ok. 9.00 rano) odwiedziliśmy małe i średnio porywające **Centro de Visitantes**, czego największym plusem było otrzymanie mapy parku. Mając już ją w ręku, skierowaliśmy się na pierwszą z wyznaczonych ścieżek.

Zielony szlak ma charakter typowo łącznikowy, wiedzie przez las do stacji **Estación Cataratas**. W tym miejscu stajemy przed wyborem: wsiadać do kolejki jadącej na skraj parku, czy wejść na Górny Szlak. Zdecydowaliśmy się na drugą opcję, bo najbardziej spektakularną atrakcję, do której prowadzę tory, chcieliśmy zostawić sobie na koniec.

### Górny Szlak (*Paseo Superior*, 1750 m)

I nareszcie dochodzimy do wodospadów, czyli sprawców tego całego zamieszania. Z tego szlaku, jak sama nazwa wskazuje, widać kaskady z góry. Miejscami można mieć wrażenie, że woda spada w przepaść wprost spod Twoich stóp! Duże wrażenie robi także panorama kanionu rzeki Iguazu oraz ciągnące się po horyzont rozlewiska.

{% include post-carousel-folder.html folder="iguazu" images="05" alt="Iguazu Falls Upper Trail" ratio="2 / 3" %}


### Dolny Szlak (*Circuito Inferior*, 1400 m)

Naszym zdaniem ta trasa zapewnia więcej wrażeń. Trzymając się niżej, faktycznie widać wodospady w całej okazałości, a jeden z nich, **Salto Bossetti**, znajduje się tak blisko ścieżki, że na podchodzących do krawędzi turystów pryska woda.

{% include post-carousel-folder.html folder="iguazu" images="10" alt="Iguazu Falls Lower Trail" ratio="2 / 3" %}


### Diabelska Gardziel (*Garganta del Diablo*, 1100 m)

Zdecydowanie najbardziej imponujący jęzor wodospadu. Już dla niego samego warto wykupić bilet wstępu do Parku Narodowego Iguazu, a pamiętajmy, że towarzyszy mu ponad 200 mniejszych kaskad.

By dostać się do Gardzieli należy wsiąść do kursującej co pół godziny kolejki (nie ma dodatkowych opłat za przejazd). Minusem tego rozwiązania jest kumulacja turystów, którzy docierają tu w dużych grupach naraz. Po przyjeździe na miejsce zatrzymaliśmy się na 15 minut, żeby tłum się rozluźnił, ale i tak większego skupiska ludzi nie doświadczyliśmy nigdzie w parku.

Kładka prowadzi do miejsca, w którym woda spada z ogromnym impetem z wysokości **82 metrów**. W powietrzu unoszą się krople rozbryzgiwanie przez wodospad, z każdej strony tworzą się tęcze, a widok trudno opisać inaczej niż “zapierający dech w piersiach”.   

{% include post-carousel-folder.html folder="iguazu" images="cover,27,21" alt="Iguazu Falls Devil's Throat" ratio="2 / 3" ratio="2 / 3" %}


### Zwierzęta Iguazu

Oprócz wodospadów, w Parku Narodowym Iguazu zachwyciły nas zwierzęta. Spotkaliśmy całe mnóstwo egzotycznych ptaków, w tym **tukana** i prześliczne **modrowronki pluszogłowe** (nazwa równie urocza jak wygląd), **jaszczurki** oraz **motyle** we wszystkich kolorach tęczy, które chętnie na nas siadały.

{% include post-carousel-folder.html folder="iguazu" images="16,15" alt="Iguazu Falls bird" ratio="1" %}


**Koati**, zwane też ostronosami, lubiliśmy do momentu, kiedy te wredne chytruski ukradły nam ze stołu pół paczki krakersów.

{% include post-carousel-folder.html folder="iguazu" images="13,12" alt="Iguazu Falls koati" ratio="2 / 3" %}


Po tym wszystkim, mimo napadu koati, uznaliśmy Park Narodowy Iguazu za jedno z niezaprzeczalnie najpiękniejszych miejsc, jakie zdarzyło nam się zobaczyć. Mieliśmy wysokie oczekiwania, ale zostały spełnione w 110%.

## Informacje praktyczne
- Bilet wstępu do Parku Narodowego Iguazu dla obcokrajowca kosztuje **800 ARS**. Jeżeli ktoś chce przyjść ponowni kolejnego dnia, można go przedłużyć za pół ceny.
- Dojechaliśmy tu autostopem, ale spokojnie, kursują tu również regularne autobusy z pobliskiego miasteczka Puerto Iguazu. Kosztują ok. **200 ARS**.
- Nie sposób się tutaj zgubić. Na szlakach obowiązuje **ruch jednostronny**, przy wszystkich skrzyżowaniach umieszczono strzałki. Na mapie lub mijanych tablicach informacyjnych można na bieżąco sprawdzać nazwy wodospadów, nad którymi aktualnie się przechodzi.
- **Infrastrukturę** oceniamy jak najbardziej pozytywnie. Nie brakuje łazienek, punktów gastronomicznych (dość drogich), a nawet dyspenserów z zimną i ciepłą wodą. Dlaczego ciepłą? Bo prawie każdy pracownik i sporo odwiedzających nosi ze sobą termos na yerba mate.
- Mieszkające w parku koati i małpki kapucynki, mogą być niebezpieczne dla człowieka. Nie należy próbować ich głaskać, ani dokarmiać (my naprawdę staraliśmy się ich nie zachęcać, ale i tak zostaliśmy obrabowani z przekąski…).


{% include post-gallery-folder.html folder="iguazu" images="01,02,03,04,05,06,07,10,22,23,25,26,11,12,13,14,15,16,17,18,19,20,21,27,cover,cover-wide" collage1="10" collage2="16" collage3="22" %}