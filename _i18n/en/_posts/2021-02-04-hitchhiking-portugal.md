---
title: "Hitchhiking in Portugal"
image: "/assets/images/hh-portugal/cover.jpg"
categories:
- categories.portugal
- categories.practical
featured: false
---


**We reached this edge of Europe for the first time in 2020, and taking into account the negative experiences of hitchhiking in Spain and the coronavirus lurking in the background, we didn't have much hope of reaching our destination fast. Nevertheless, we took up the challenge and it went much better than we expected. If you want to find out how hitchhiking works in Portugal (at least from our experience), read on.**

<!-- more -->

## In brief
**Days**: 13 (maybe 3-4 of them are actually on the road) \\
**Distance traveled**: 1100 km \\
**Drivers**: 22 \\
**Longest distance traveled in one car**: 460 km (Ourique-Porto) \\
**Shortest wait**: 0 minutes (the driver stopped when we weren't even hitchhiking) \\
**Longest wait**: 3 hours \\
**Median**: ~40 minutes

## Our route
However weird it may sound, we ended up in Faro, Portugal for purely practical reasons. When evacuating from South America, we chose the cheapest of the few available flights from São Paulo.[ We wrote more about lockdown in Paraguay and the difficult return to Europe here](/en/posts/bad-luck).
Aside from that, we hadn't had a chance to visit Portugal before, so the choice was even easier.

From Faro, we set out on a route along the coast to Cape St. Vincent, admiring the interesting parts of the Algarve region along the way. Then we drove through almost the whole country, reaching Porto. The last point on our route was the much less known city of Aveiro, with the adjacent resort called Costa Nova. Finally, the A25 motorway brought us to the border with Spain.

Apart from public transport in Porto, we hitchhiked the entire distance.

{% include post-carousel-folder.html folder="hh-portugal" images="6" alt="Łapanie stopa w portugalii" ratio="1" %}

## Hitchhiking in Portugal - our feelings
### Familiarity with the idea of ​​hitchhiking
In this respect, Portugal did not stand out from most European countries - the drivers understood the "thumbs up" gesture, no one asked for payment, nor insisted on dropping us to the train station. We haven't seen any other hitchhikers, but the general decline in tourist traffic in 2020 was probably to blame for that.

### Drivers
Slightly more than half of the drivers we traveled with in Portugal were locals and **the vast majority spoke English**, which made communication much easier. In Algarve we drove several times with tourists from various other parts of the world. We even came across two guys who stopped to pick us up, even though we didn't even start hitchhiking. One of them was from Germany, the other from the US and they came there to spend the summer in one of the local hippie camps.

### Hitchhiking spots
We had to march quite far outside the city relatively often, because there were no safe places to stop. To get out of Porto, we took a bus to the roundabout recommended for Hitchwiki, but to be honest, it wasn't great, so we don't recommend it.
As for other spots, the gas stations on the A25 highway were pretty terrible, we advise against them.

{% include post-carousel-folder.html folder="hh-portugal" images="5" alt="Hitchhiking in Portugal" ratio="2 / 3" %}

### Movement speed
We covered the route along the Algarve coast very slowly, but not because of poor hitchhiking, but because of our own choice. In practice, we moved from town to town without waiting more than 20-30 minutes. Further on, it was unfortunately a lot worse and we got to Porto efficiently only thanks to a guy that took us straight there after a whole day of bad luck.

## Overall rating:

{% include post-thumbs.html rating=4 %}

In our opinion, hitchhiking in Portugal works much better than in Spain. In general, we took into consideration the fact that, despite the very few cases of COVID during our trip, fewer people traveled longer distances. Others, understandably, simply avoided contact with strangers.
We would definitely like to visit Portugal again and see how it feels to hitchhike in normal times.

## Sleeping
In Porto we stayed with a friend from university, but as a rule we **slept in a tent**. Finding secluded places was not a big problem anywhere, and in the Algarve it was even an attraction in itself. We spent a few nights on the cliffs, sheltered by vegetation or rocks, and one night on a small empty beach (we didn't even pitch our tent).

{% include post-carousel-folder.html folder="hh-portugal" images="1" alt="Camping in portugal" ratio="2 / 3" %}

## Food
### Shopping
After four months in South America, we missed Lidl, Aldi, Intermarche… Seriously, our first visit to a supermarket with products we knew genuinely made us happy. Prices are a bit higher than in Poland, but they are still far from the Spanish and French.

### Eating out
We mainly cooked ourselves using our reliable burner. From the pubs, we only remember **Almada Cafe** in the center of Porto, where we ate delicious *bolinhos de bacalhau* (cod and potato meatballs) and *francesinha* (a typical Porto casserole made of bread and an absurd amount of meat in tomato-beer sauce).

{% include post-carousel-folder.html folder="hh-portugal" images="4,3" alt="Food in Portugal" ratio="2 / 3" captions="Francesinha in Almada Cafe; Pastéis de nata" %}

## Security
In fact, throughout Europe, and certainly in its EU part, we feel "at home", so we are constantly accompanied by a blissful sense of security. During these two weeks, the only situation during which I felt discomfort \[W.\] was sleeping in a tent by the fence separating the hunting grounds.

\* \* \*

The hitchhiking trip through Portugal was very quick and pleasant. We recommend it without hesitation to all lovers of alternative forms of travel. We will definitely come back, for example to Lisbon, which we still know only from photos.