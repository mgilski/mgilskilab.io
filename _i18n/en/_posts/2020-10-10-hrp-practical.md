---
title: "How to survive in the Pyrenees? Practical information"
image: "/assets/images/hrp-practical/cover.jpg"
categories:
- categories.spain
- categories.france
- categories.andorra
- categories.practical
featured: false
---

**Because of the fact that this was our first long distance trail, we could undoubtedly have done many things much better. Nevertheless, we managed to complete the Haute Randonnée Pyrénéenne (HRP) running through the entire range without hurting ourselves. This post can be treated as tips for other hikers, but also as answers to the most frequently asked questions.**

<!-- more -->

## Preparation
This is of course an individual matter, but for us, the HRP trail didn’t involve any longer preparation. We made the decision spontaneously, our backpacks were ready and our physical condition wasn’t too bad. The only thing left to do was read about the route and the expected weather conditions, and we could be on our way.

### Good timing
Basically, the best season in the Pyrenees is **from mid June to late September**. Those who like solitude in the mountains can probably choose other months, but the HRP trail then becomes a completely different kind of challenge. The snow begins to fall early and doesn’t melt for a long time. Many mountain passes are already impassable in autumn without a ton of equipment. From experience, we can say that between July 16 and September 2, the conditions for hiking are great, or at least they were in 2020.

### Good equipment
1. When deciding on the choice / purchase of equipment for the HRP trail, it is worth remembering a few issues.
It can be really hot in the Pyrenees in summer, especially in the valleys, but above 2,000 meters above sea level the temperature can drop to just **a few degrees Celsius** at night.
2. Low approach shoes turned out to be perfect for us. However, you have to bear in mind that even in the middle of summer, there are sections covered in snow, so without higher boots or gaiters, you have to walk very carefully not to get snow in your shoes.
3. Unless you’re a cyborg that can walk 800 km in three weeks or less (we met a few on the route) then you need a **tent** as there are no other accommodation options at some stages. As we know from the stories of hikers we met, 800-gram tarps pitched up with trekking poles may not survive the night storms that haunt the Pyrenees pretty often.
4. If someone has a different idea, feel free to share it in the comments, but in our opinion **it is impossible to do the HRP without a solar charger**. With our small set of panels we managed to charge a telephone, camera, e-book reader and powerbank. We literally used "regular" electricity from the socket only once.
5. Your backpack must contain not only the equipment you take from home, but also a lot of **food and water**. Sometimes even 5-6 days’ worth of supplies are necessary (again, if you’re not running the trail). Fortunately, there are plenty of water sources and other places to fill a bottle for most of the trail.

{% include post-carousel-folder.html folder="hrp-practical" images="10" alt="Krowa na szlaku HRP" ratio="2 / 3" captions="Solar charger attached to backpack" %}

What exactly did we take? **Don't follow our example!** In our backpacks, we had **everything we took for our trip around the world** - a lot of unnecessary clothes and other stuff, including a laptop. We decided that in the worst case we would simply send the excess back to Poland, but thanks to our amazing backpacks, we basically didn’t complain about having to carry the additional stuff at all (even though Maciej’s backpack weighed 20kg sometimes).

### Good map
Warning! The HRP itself **is not marked**. When it connects to other trails, you can rely on markers, but most of the time, a map is essential. We relied on **Paul Atkinson’s** guide, which we downloaded as an e-book (from [here](https://whiteburnswanderings.wordpress.com/2018/12/28/hrp-pocket-guide-rev-1/), includes the guide in doc format and a gpx file). We supplemented verbal descriptions such as "after 2 km turn left into a forest path" or worse, "improvise your way down", with two applications - mapy.cz and maps.me with a GPS track loaded. This combination worked very well because we only got lost a few times and it was always as a result of not paying attention.

{% include post-carousel-folder.html folder="hrp-practical" images="9" alt="Mapa HRP" ratio="1" captions="Weronika checking the guide" %}

## Life on the trail
Remember that we’re describing our own experiences, not a ready recipe for the HRP trail. Keep in mind that we had no deadline, and were carrying heavy backpacks out of pure stubbornness.

### What to eat?
Roots? Mushrooms? Pinecones? There was no need to enter survival mode. We used the gifts of the mountains quite often, but mainly in the form of raspberries and blueberries, which grow abundantly in the Pyrenees. From the guide, we knew how many kilometers there was between villages (sometimes 50, sometimes 100). After the first stop, we came up with a great system. Instead of buying in local, terribly expensive and usually small shops, we did a half-day chillout, during which Maciej **hitchhiked to a supermarket** in a bigger city.

We both have a weakness for good food in big amounts. Immediately after shopping, we had real feasts, with cake, fruit, chips and drinks other than spring water. Between the shopping stops, we ate mainly peanuts, chocolate and couscous or pasta with various additions. By the end, we stopped caring about the weight, so we carried less weight optimized and more varied snacks, from chocolate milk to gummy bears.

{% include post-carousel-folder.html folder="hrp-practical" images="5,4" alt="Jedzenie na szlaku HRP" ratio="2 / 3" captions="Mac and cheese - the best dinner; Awesome salad mixed directly in the bag" %}

### Where to sleep?
As we mentioned, **a tent is absolutely necessary**. Camping is absolutely allowed, even in national parks, where it’s only prohibited from 9.00 to 19.00. In addition, **generally accessible cottages** were marked in our guide. Their standard varies, but in each of them you can count on a roof over your head and a bunk bed for sleeping. We slept in them around five times. There were usually no problems, except for one night, when we fought an army of mice and ended up losing a bar of chocolate. The third accommodation option are **mountain refuges**, but there are relatively few of them and almost all of them need to be booked in advance during the holidays.

{% include post-carousel-folder.html folder="hrp-practical" images="1,7,8" alt="Spanie na szlaku HRP" ratio="2 / 3" %}

### How not to smell bad?
We washed ourselves in rivers, streams and lakes, mainly using a method of intensive scrubbing, in order to avoid adding soap to the ecosystem. We encountered more or less civilized bathrooms in shelters and in towns along the trail.

We had so many clothes with us that we didn't have to wash them by hand in the sinks and water reservoirs we encountered. Just after sweating all our clothes, we visited a laundromat, usually adjacent to a supermarket. Another popular (and lighter) way is to take only two sets of clothes and wear one while the other one dries after washing.
 
### How to communicate?
We felt comfortable in the Pyrenees, because one of us speaks **Spanish**, and the other one **French**. However, don’t worry if you don’t speak any of these languages. Almost every person with a backpack we met knew **English** and it would probably be easy to find someone who will help you communicate with the shop or shelter staff.

## Pyrenean dangers
**No way to call for help**
There is really no reception in these mountains. Do you know the "emergency calls only" you get when the signal is very weak? Usually there wasn't even that on the HRP trail. There were sections where there was no coverage for five straight days.

**Poor visibility**
The western part of the trail is often covered with low clouds, making navigation much more difficult. It is easy not only to get lost, but also to walk straight into a cow emerging from the fog.

{% include post-carousel-folder.html folder="hrp-practical" images="2" alt="Krowa na szlaku HRP" ratio="1" %}

**Sudden storms**
For several nights, our 6-year-old tent was very thoroughly strength-tested. Lightning storms combined with strong wind, downpour and hail are one of the greatest threats to wanderers.

**Dog Fangs**
Horses, cows and sheep are an integral part of the local landscape. They are usually guarded by formidable Pyrenean mountain dogs, called *patou* (males usually weigh 50-60 kg). If they don't like you, which happened to us once, they can growl and bark very effectively, and even snap at the bottom of the backpack. Their goal, of course, is not to harm, but to scare, and they do it very well.

{% include post-carousel-folder.html folder="hrp-practical" images="3" alt="Patou na szlaku HRP" ratio="2 / 3" captions="The two cute dogs that almost scared us to death just 3 minutes before the photo was taken" %}

## Sample prices
Our main expense was grocery shopping in supermarkets. Everyone can check how much pasta costs in the French Intermarche (more than in Poland, but not excessively), so we're not going to write about it. However, it’s worth mentioning the  **prices of mountain refuges**, because such information is a bit more difficult to reach. Interestingly, in terms of spending, we hardly noticed any difference between France and Spain.

- accommodation without meals - 15-20 €
- dish of the day or a plate of spaghetti- € 9-10
- large sandwich - 5-6 €
- coffee - 1.5-3 €
- piece of cake - 3-5 €

\* \* \*

We spent 47 days in the Pyrenees, going from the Atlantic Ocean (Hendaye) to the Mediterranean Sea (Banyuls-sur-Mer). We walked slowly, taking long breaks whenever we felt like it. The practical information here concerns a very recreational hike of the HRP trail. Unfortunately, we can’t give you advice on whether you need two or three technical T-shirts or what are the most calorie-efficient snacks you can buy. Nevertheless, we keep our fingers crossed for everyone who wants to test their physical and mental strength on the HRP!

**If you want to see photos from our trip, be sure to take a look at our [HRP trail gallery post](/en/posts/hrp-gallery)!**