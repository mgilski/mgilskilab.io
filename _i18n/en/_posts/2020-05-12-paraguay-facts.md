---
title: "7 things that surprised us in Paraguay"
image: "/assets/images/paraguay-facts/cover.jpg"
categories:
- categories.paraguay
- categories.thoughts
featured: false
---


**During our two months in Paraguay, few things amazed us, but there was a lot that surprised. This was the first non-European country where we had to spend so much time, so a great opportunity to get aquainted with the local reality  (of course, as far as possible given the pandemic).**

<!-- more -->

## 1. Bilingualism
Prior to arrival, we knew from Wikipedia that the official language is, next to Spanish, _**guarani**_. However, after some conversations with locals, we discovered the actual popularity of this language. Guarani is used not only by people of Indian and mixed origin, but also by descendants of immigrants. In total, **90% of the population**! Let there be no doubt - the rest simply use other native languages, and as much as 13% don't speak Spanish at all.

## 2. Yerba mate like water
We already had a taste of this phenomenon in the Argentinian province of Misiones, but it is not an accident that yerba mate is officially called ***Ilex paraguariensis***. Inhabitants of the country seem to drink the infusion of this plant from dawn to night. Literally everywhere, you can see people carrying a thermos with hot (or cold) water for the next brews. Cold _mate_, or **tereré**, is very popular. The drink is so popular, that the government specifically urges not to share is using the same bombilla (a metal tube with a strainer) during the coronavirus epidemic.

{% include post-carousel-folder.html folder="paraguay-facts" images="3" alt="Yerba mate in Paraguay" ratio="1" %}

## 3. Sopa paraguaya
Sopa paraguaya belongs to the local national dishes. What's interesting about is, is that the soup [Spanish *sopa*] is one by name only. In fact, it's something more like a sponge casserole made from corn flour, eggs, milk, onions and cheese. So where does this soup come from? According to legend, the dish was "accidentally invented" by a chef who actually wanted to prepare it in liquid form, but added way too much flour.

## 4. Overproduction of foil
Here is a very negative surprise for gringos like us. **The culture of limiting plastic use** has clearly not reached Paraguayan stores. If you do not object in advance, the service will pack your purchases into a plastic bag for sure. The most absurd situation was when the cachier offered us a plastic bag when buying a single article, which was... peanuts by weight, already in a plastic bag.

## 5. Disproportions in product prices
We came to Paraguay with an epectation that it would be really cheap here, without comparison with Brazil. We don't know where the authors of these opinions shopped, but in both countries we spent about the same amount on food. Indeed, some groceries have nice prices. You can save a lot by buying products such as flour, rice, pasta or corn **by weight**. Breakfast cereals, chocolate, we bought cheaper in countries considered more expensive, such as the United Kingdom or France, and imported brand products like nutella cost up to 4x more than in Poland.

## 6. Everyday Is Christmas
Huge Christmas trees on roundabouts? Chains with bells above the entrance? Reindeer decorations on the door? We saw all this in Paraguay **in April** and we have no doubt that the situation remains unchanged throughout the year. We asked one driver about it, and we received an obvious answer that it makes no sense to remove the decorations, since in a few months you would have to put them back up again. Well, Paraguayans definitely seem like a very chilled out nation.

{% include post-carousel-folder.html folder="paraguay-facts" images="2,1" alt="Święta w Paragwaju" ratio="1" %}

## 7. Comfortable sleeping
When paying 4 USD per night, there is no point in complaining about dirty walls with peeling paint, old furniture or the lack of windows. We were surprised, however, that regardless of the standard of the rest of the room, in all places we had the opportunity to sleep, there were really comfortable mattresses. It is difficult to say why the owners invest specifically in this item of equipment, but it suits us, because there is nothing like a good night's sleep after an active day.

{% include post-carousel-folder.html folder="paraguay-facts" images="4" alt="Sleeping in Paraguay" ratio="1" %}