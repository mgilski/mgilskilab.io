---
title: "A hitchhiker's guide to Brazil"
image: "/assets/images/hh-brazil/10.jpg"
categories:
- categories.brazil
- categories.practical
- categories.articles
featured: false
---

**Brazil is the 42nd country where we tried hitchhiking. Three weeks is definitely not enough to see such a huge, diverse country, so we limited ourselves to the states of São Paulo, Rio de Janeiro and Paraná. The report from our hitchhiking experiences applies only to this area.**

<!-- more -->

## Stats

**Days in Brazil:** 21  
**Distance traveled:** 2,000 km  
**Drivers:** 22  
**The longest distance covered in one car:** 290 km (Paraty-São Paulo)  
**Shortest wait:** 5 minutes  
**Longest wait:** 4 hours  
**Median:** 50 minutes

## Our route

We arrived from Europe to the **Guarulhos airport** in the suburbs of São Paulo, from where we went to **Rio de Janeiro** via the BR-116 highway (Rodovia Presidente Dutra). After a few carnival days we returned on the road, this time riding along **Costa Verde** to the town **Paraty**, and then back to **São Paulo**. In the state of Paraná we stayed in **Curitiba** and in **Foz do Iguaçu**.

Apart from moving around cities, we didn't use any other means of transport than hitchhiking.

{% include post-carousel-folder.html folder="hh-brazil" images="map" alt="Our route in Brazil" ratio="2 / 3" %}


## Hitchhiking in Brazil - our feelings

### Understanding of the idea of ​​hitchhiking
We didn't meet other hitchhikers, but the drivers clearly understood what we mean by standing by the road with our thumbs up, not to mention cardboard signs. Many people know about hitchhiking in Brazil thanks to **policemen and firefighters** who oftentimes return from work this way. We have actually seen them do it!

A couple of taxis and even more Ubers stopped for us, but they understood our intentions right away after we used the word *carona* (portugese for ride, hitchhiking). We were offered a free ride twice, so explaining paid off.

{% include post-carousel-folder.html folder="hh-brazil" images="10" alt="Decorations in local trucks" ratio="2 / 3" %}

### Drivers
With one exception, we got rides only from Brazillians, not foreign tourists. The most common vehicles were by far  **passenger cars**, but there were also four trucks, one camper and one... school bus.

All drivers were friendly, although contact was significantly hindered by the **language barrier**. They understood our broken Spanish quite well, but for us Portuguese sounded like alien speech at the beginning. Fortunately, along the way we got used to pronunciation and, with a little help from Google Translate, conversations were not too bad.

We read that few Brazilians speak English except in Rio de Janeiro and São Paulo. That’s not exactly true: even in these cities, they’re also almost impossible to find. We have definitely encountered the most English-speaking residents in Curitiba. We rate foreign language skills by drivers 1/10.

{% include post-carousel-folder.html folder="hh-brazil" images="3,1" alt="Our dog companions in Brazil" captions="Zeus the dog; Zeus's adopted kids" ratio="2 / 3" %}

### Hitchhiking spots
Good hitchhiking spots usually depend on the road type and hitchhiking in Brazil is no different in this respect from other countries.
- **Highways** - gas stations appear regularly, most of them have one exit, making it easier to catch a ride. We were very positively surprised by the cleanliness of the bathrooms and the quite common presence of showers. The highways are tolled, so you can also hitchhike at the toll stations. The police are often stationed there, but they don’t mind and might even help.
- **Other roads** - we did not travel off the main roads, so we’re talking only about well-maintained inter-city roads. The best places to catch a ride were bus stops, but most of the sections we traveled had wide shoulders, where it was also safe to stop.

{% include post-carousel-folder.html folder="hh-brazil" images="2,4" alt="Brazilian roads" ratio="2 / 3" %}

### Speed ​​of movement
We were in no hurry, but we would’nt complain if hitchhiking in Brazil went a bit faster. We often waited for over an hour, and a lot of times we only covered 10-20 km with one driver.

Our current daily record is almost 1500 km (Orlean-Poznań), while in Brazil we never exceeded 400 km on any day.

## Hitchhikability:

{% include post-thumbs.html rating=3 %}


We met a lot of awesome people who, seeing a pair of sweaty white people with backpacks, offered us advice, water, snacks, and in two cases even accommodation. Because of that, we were pretty surprised that hitchhiking in Brazil works at most **average**.


We attribute this mainly to the common **fear**. Brazilians themselves think their country is dangerous, and taking a stranger into your car can be considered an unnecessary risk.

## Sleeping

### Cheap accommodation
Very cheap accommodation with a decent standard is easy to find on Airbnb. We didn't have many opportunities to roam the cities in search of accommodation, but several times we saw the price of 50 BRL per person, so you can certainly find a reasonable accommodation also without booking.

### Wild camping
We spent most of the night in our tent, wherever we were when it got dark. Sometimes finding a piece of flat terrain was not easy, because the part of Brazil that we visited is overgrown with very **dense tropical vegetation**. We sometimes pitched our tent at gas stations and the staff did not pay any attention to us.

{% include post-carousel-folder.html folder="hh-brazil" images="12" alt="Camping in Brazil" ratio="3 / 2" %}

## Food
### Shopping
During our stay in Brazil, we regularly had access to the kitchen, plus we also had a portable stove, so we were also able to cook in our tent.

Prices of basic food products do not differ much from Polish ones (Poland is one of the cheapest countries in Europe). Exceptions:
- **very cheap:** mango, green pepper (other colors cost much more), eggs, carbonated drinks (unbranded)
- **very expensive:** bread other than toasted bread, couscous, chocolate, chips, peanut butter, most dairy products

### Eating out
Brazilians eat out during lunchtime. The most popular restaurants are *por kilo*, where you pay by weight, sometimes also with an option of all you can eat. We prefer to eat the main meal of the day early in the evening, so we’ve only eaten out once or twice, but we tried many popular snacks here.

{% include post-carousel-folder.html folder="hh-brazil" images="5,6,8,9,11" alt="Snacks in Brazil" captions="Salgado + refresco - classic street food: a salty snack and a cold drink; Churros - usually with chocolate or dulce de leche; Coxinha - crispy dumplings with chicken or other fillings; Pão de queijo - cheese bread made from flour and cassava; Pastel - rectangular deep-fried snack, usually filled with meat" ratio="2 / 3" %}

On the podium of the tastiest dishes we place sandwiches and ribs from Estadão in São Paulo and tapioca pancakes made by our Brazilian mom Patricia ❤

{% include post-carousel-folder.html folder="hh-brazil" images="7" alt="Dinner in Sao Paulo" ratio="2 / 3" %}

## Safety
Many travelers more experienced than us have already written about security issues in Brazil. In large cities it is relatively easy to say goodbye to your wallet, camera, or even worse.

Contrary to appearances, we don’t lack common sense. We tried not to wander at night, we passed the favelas with a wide arc, we didn’t show off our electronic equipment, etc. We haven’t encountered any crime, but this does not change the fact that it occasionally affects tourists.

Despite all that, it’s also true that Brazilians are creating an atmosphere of danger on their own. Almost every day we heard sentences like "If you go to Rio, don't go anywhere in the evening", "Are you not afraid?", "You better hide your phone." The latter was said when in the middle of the day we stood next to a church, shielded on both sides by walls, with a group of policemen in sight. So yeah.

