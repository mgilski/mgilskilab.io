---
title: "The 275 cascades of Iguazu"
image: "/assets/images/iguazu/cover.jpg"
feature_text: "The 275 cascades of Iguazu"
categories:
- categories.argentina
featured: false
---

**Do you like waterfalls? We certainly do! The Iguazu National Park takes this concept to a whole new level. Thanks to the variety of marked routes, you can watch these wonders of nature from several different perspectives, so surprisingly, staring at water becomes an attraction for a whole day.**

<!-- more -->

## The largest or one of the largest?

We still haven't figured out which one it is. Until recently, we thought Iguazu was the **largest waterfall in the world** (that's what the Wikipedia article states), but after a bit of research, it turned out that it's neither the widest, nor the one with the largest flow. What there is no doubt about is that it beats both Niagara and Victoria falls in the second category by 100%.

*If somebody knows why Iguazu is widely considered the biggest on earth, please let us know in the comments, we would like to know.*

It is worth noting that the river Iguazu, during its spectacular drop, is divided into 275 cascades, with a total width of 2.7 km, and height between 60 and 82 meters, hence the quite unsophisticated, but very apt name in Guarani *Yguasu*, i.e. Great Water.

## Argentina vs. Brazil

The Iguazu Falls are located on the border between these two countries (and a stone's throw from Paraguay by the way). 80% of the area is in Argentina, 20% in Brazil. Both sides are protected by national parks inscribed on the **UNESCO World Heritage List**.

Tourists with limited time or baudget (that's us) usually decide to see only the Argentinian part. It has a more extensive network of routes, and you can get closer to the waterfalls. We chose this variant without hesitation and after a day in the national park, we felt completely satisfied and had no need to go dee the Brazilian side.

## What can you see here?

The Iguazu National Park covers not only the area of ​​waterfalls, but also the extensive rainforest rich in flora and fauna. The scenery is like from a fairy tale! Even the number of visitors was reasonable, which came as a nice surprise to us. There were fragments where we could walk a couple hundred meters and meet no-one (except for animals, of course).

Our walk in the park was as follows...

### Green trail (*Sendero Verde*, 600 m)

After buying the tickets (there were almost no queues, andwe went in at around 9.00 am), we visited the small and not very interesting **Centro de Visitantes**, where we got the park maps. With that in hand, we headed to the first of the designated paths.

The green trail connects the park entrance to the other trails. It leads through the forest to the **Estación Cataratas** station. At this point, we had to face a choice: get on the train to the edge of the park, or follow the Upper Trail. We decided on the second option, because the most spectacular attraction, we wanted to asve the best for later.

### Upper Trail (*Paseo Superior*, 1750 m)

This is the point when we finally get to the waterfalls. From this trail, as the name implies, you can see cascades from above. In some places you can have the impression that the water is falling into the abyss from right under your feet! The panorama of the Iguazu River Canyon and the backwaters stretching to the horizon are also very impressive.

{% include post-carousel-folder.html folder="iguazu" images="05" alt="Iguazu Falls Upper Trail" ratio="2 / 3" %}


### Lower Trail (*Circuito Inferior*, 1,400 m)

In our opinion, this is the more spectacular route of the two. By staying below, you can actually see the waterfalls in all their glory, and one of them, **Salto Bossetti**, is so close to the path,
that you get sprayed by water if you get too close.

{% include post-carousel-folder.html folder="iguazu" images="10" alt="Iguazu Falls Lower Trail" ratio="2 / 3" %}


### Devil's Gorge (*Garganta del Diablo*, 1100 m)

By far the most impressive cascade of the falls. It is worth buying a ticket just to see it alone, and remember that it is accompanied by over 200 smaller ones.

To get to the Throat you should take the train that runs every half hour (there are no additional fees). The downside of this solution is the accumulation of tourists who get there in large groups at once. After arriving at the place, we waited for 15 minutes for the crowd to loosen up, but even then, there were more people than anywhere in the park.

The footbridge leads to a place where the water falls from **82 meters** with huge momentum. There's water splashing everyoneand rainbow everywhere., The view is difficult to describe otherwise than "breathtaking".

{% include post-carousel-folder.html folder="iguazu" images="cover,27,21" alt="Iguazu Falls Devil's Throat" ratio="2 / 3" %}


### Iguazu animals

Aside from the waterfalls, the Iguazu National Park also delighted us with animals. We met a whole lot of exotic birds, including **toucans**, lovely **plush-crested jays**, **lizards** and colorful **butterflies**, hwich even landed on us.

{% include post-carousel-folder.html folder="iguazu" images="16,15" alt="Iguazu Falls bird" ratio="1" %}


We liked **coatis**, also called hog-nosed coons, until those little thieves stole half a packet of crackers from the table.

{% include post-carousel-folder.html folder="iguazu" images="13,12" alt="Iguazu Falls koati" ratio="2 / 3" %}


In the end, despite the coati attack, we think the Iguazu National Park is undeniably one of the most beautiful places we have ever seen. We had high expectations, but they were met in 110%.

## Practical information
- The admission ticket to the Iguazu National Park for a foreigner costs **800 ARS**. If someone wants to come again the next day, it can be extended for half the price.
- We hitchhiked here, but there are also regular buses from the nearby town of Puerto Iguazu. They cost around **200 ARS**.
- You can't get lost here. The trails are **one-way**, arrows are placed at every intersection. On the map and information boards you can check the names of the waterfalls you are currently seeing.
- The infrastructure is very good. There are plenty of bathrooms, sandwich stands (quite expensive), and even dispensers with hot and cold water. Why hot? Because almost every employee and lots of visitors carry a thermos for yerba mate with them.
- Coatis and capuchin monkeys living in the park can be dangerous to humans. Don't try to stroke or feed them (we really tried not to encourage them, but we still were robbed of a snack...).

{% include post-gallery-folder.html folder="iguazu" images="01,02,03,04,05,06,07,10,22,23,25,26,11,12,13,14,15,16,17,18,19,20,21,27,cover,cover-wide" collage1="10" collage2="16" collage3="22" %}