---
title: "How we visited 41 countries - our travels 2014-2019"
categories:
  - categories.thoughts
  - categories.articles
image: "/assets/images/prev/cover.jpg"
feature_text: "Overview of our previous travels"
featured: false
---



**We started this blog to talk about our current hitchhiking trip through the world. This does not mean, of course, that we have nothing to say about previous travel experiences! To keep you from waiting for new posts, we invite you all to a small summary of our previous trips abroad.**

<!-- more -->

## 2014
### Spain
To this day it is hard to say how the trip came into existance. We've been a couple for only two months, Wera was underage, and we didn't have any experience hitchhiking outside of Poland. Without smartphones, but with a decent European road atlas, we travelled from Poznań to the Costa Blanca, to Altea, and back.

**Coolest experience**: eating paella on the beach, straight from the pan

**The weirdest experience**: sleeping in a park in the center of Valencia and being woken up by sprinklers in the morning

{% capture alt%} {% t alt.spain%} {% endcapture%}
{% include post-carousel.html images = "assets/images/prev/74.jpg" alt = alt ratio="3 / 4" %}


## 2015
### Amsterdam - Brussels - Luxembourg
Taking advantage of the holidays, we visited the Benelux capitals. We took a guitar with us experimentally and, despite at most average musical abilities, we earned quite an impressive amount playing on the streets. After approx. 4 hours we had enough for the whole month trip to Georgia and Armenia (more on that below).

**The coolest experience**: an evening walk through the Luxembourg Parc des Trois Glands with fireflies flying around us


**The weirdest experience**: hitchhiking with a Dutchman smoking local goods behind the wheel

{% capture alt%} {% t alt.amsterdam%} {% endcapture%}
{% include post-carousel.html images="assets/images/prev/8.jpg, assets/images/prev/7.jpg, assets/images/prev/cover.jpg" alt=alt ratio="2 / 3" %}


### Georgia and Armenia
The plan of this expedition was to reach the Caucasus mountains by a route through the Balkans and Turkey. Along the way, we were delighted by the picturesque Kotor Bay in Montenegro, Albanian landscapes, as well as the crazy markets of Istanbul. For the first time, our set of foreign languages ​​(English, Spanish and French) turned out to be insufficient, so for a month we communicated by interweaving Polish with single words in Russian. And by Google Translate.

**The coolest experience**: overwhelming Armenian hospitality

**The weirdest experience**: night downpour in Georgia, during which our tent was almost taken by an overflown river

{% capture alt%} {% t alt.georgia%} {% endcapture%}

{% include post-carousel.html images="assets/images/prev/3.jpg, assets/images/prev/2.jpg, assets/images/prev/1.jpg, assets/images/prev/4.jpg, assets/images/prev/5.jpg" alt=alt ratio="2 / 3" %}


## 2016
### Around the Baltic Sea
The name is misleading, because we not only circled the Baltic Sea, but also reached Barent's Sea, the northern edge of Europe. We crossed the magical Arctic Circle for the first time! We visited several really amazing cities - Vilnius, Riga, Tallinn, Helsinki and Stockholm. Norwegian landscapes, with fjords, mountains and reindeer herds on the roads, just brought us to our knees.

**The coolest experience**: trekking around the Oulanka National Park in Lapland

**The weirdest experience**: in Norway every second driver driving us was Polish, and one of them was additionally a Catholic priest (the only one within 600 km)

{% capture alt%} {% t alt.baltic%} {% endcapture%}
{% include post-carousel.html images="assets/images/prev/15.jpg, assets/images/prev/10.jpg, assets/images/prev/11.jpg, assets/images/prev/12.jpg, assets/images/prev/13.jpg, assets/images/prev/14.jpg, assets/images/prev/9.jpg, assets/images/prev/16.jpg, assets/images/prev/17.jpg" alt=alt ratio="2 / 3" %}

### England and Wales
Great Britain welcomed us with beautiful weather (for November) that lasted throughout the visit. Checkmate, statistics! In England we visited Bristol, Bath and the Cheddar Gorge (yes, that's where Cheddar cheese comes from), and in Wales - Cardiff and Aberystwyth.

**Coolest experience**: walks along the coast and eating fish and chips in Aberystwyth

**The weirdest experience**: the first meeting in our lives with left-hand traffic

{% capture alt%} {% t alt.england%} {% endcapture%}
{% include post-carousel.html images="assets/images/prev/67.jpg, assets/images/prev/68.jpg, assets/images/prev/69.jpg, assets/images/prev/71.jpg, assets/images/prev/72.jpg, assets/images/prev/73.jpg" alt=alt ratio="2 / 3" %}

### Israel and Palestine
The dicision about this trip was made in a quarter of an hour. This is how long it took to get out of bed, talk on the phone and buy the tickets. Together with some friends, we spent a total of 12 days in Israel, during which we walked the alleys of Jerusalem, crossed the wall to Palestine in order to reach Bethlehem, climbed the ruins of Masada and wandered the Negev desert.

**The coolest experience**: being surrounded by places taken straight from the Bible, which seemed a bit unreal

**The weirdest experience**: swimming in the Dead Sea

{% capture alt%} {% t alt.israel%} {% endcapture%}
{% include post-carousel.html images="assets/images/prev/19.jpg, assets/images/prev/18.jpg, assets/images/prev/20.jpg, assets/images/prev/21.jpg, assets/images/prev/22.jpg, assets/images/prev/23.jpg, assets/images/prev/24.jpg, assets/images/prev/25.jpg, assets/images/prev/26.jpg" alt=alt ratio="2 / 3" %}

## 2017
### Peru
Definitely the furthest of our expeditions to date. We visited the Inca country for two weeks, and thanks to the long transfers we also saw the capital of Mexico. We remember every day in the shadow of the Andean peaks. We climbed our first 5000 meter mountain, sailed on a boat on Lake Titicaca and patted the alpaca. We came back to Europe enchanted by South America, with a firm decision to return again, preferably for a much longer visit.

**The coolest experience**: trekking along the bottom of the Cotahuasi Canyon, where we met several donkeys, but no tourists

**The weirdest experience**: hitchhiking on a trailer with two horses

{% capture alt%} {% t alt.peru%} {% endcapture%}
{% include post-carousel.html images="assets/images/prev/30.jpg, assets/images/prev/28.jpg, assets/images/prev/29.jpg, assets/images/prev/27.jpg, assets/images/prev/31.jpg, assets/images/prev/32.jpg, assets/images/prev/44.jpg" alt=alt ratio="2 / 3" %}

### Iceland
We used the majority of the 2017 travel budget for a flight to Peru, which is why we went to Iceland, but with only hand luggage and virtually no money. Amazingly, somehow we managed to survive in this horrendously expensive country - mainly thanks to our tent and freeze-dried meals. It was definitely worth putting up with all the inconvenience for the northern lights and alien landscapes!

**Coolest experience**: staring for a good half day at ice drifting on the Jökulsárlón lagoon

**The weirdest experience**: night in a tent that was basically flattened from the wind

{% capture alt%} {% t alt.iceland%} {% endcapture%}
{% include post-carousel.html images="assets/images/prev/36.jpg, assets/images/prev/34.jpg, assets/images/prev/35.jpg, assets/images/prev/33.jpg, assets/images/prev/37.jpg, assets/images/prev/38.jpg, assets/images/prev/39.jpg, assets/images/prev/40.jpg, assets/images/prev/41.jpg, assets/images/prev/42.jpg, assets/images/prev/43.jpg" alt=alt ratio="2 / 3" %}

## 2018
### Hungary - Serbia - Slovenia - Bavaria
The year 2018 was dominated by project WEDDING, so we went on our first longer trip on our pseudo-honeymoon. Why pseudo? It was not a real honeymoon, but moving to France the long way around. Our route led, among others through Budapest, Belgrade, Lake Bled and the Salzburg Alps.

**The coolest experience**: an evening walk around Budapest

**The weirdest experience**: entering in the dark in the middle of a herd of cows in the Berchtesgaden National Park (this is what happens when you forget to take a flashlight...)



### Paris
Due to Maciej's studies, we spent the first 3 months of our marriage in a suburb near Paris. We did not like life in France, which is well known to everyone who had to listen to our complaints (sorry). However, we liked the city itself. We recommend going there out of season, just like we did, because crowds of tourists effectively spoil the atmosphere.

**The coolest experience**: visiting the Louvre from opening to closing

**The weirdest experience**: struggling with French bureaucracy

{% capture alt%} {% t alt.paris%} {% endcapture%}
{% include post-carousel.html images="assets/images/prev/64.jpg, assets/images/prev/63.jpg, assets/images/prev/65.jpg" alt=alt ratio="2 / 3" %}

## 2019
### Northern Germany
Together with a couple of friends, we decided to visit Miniatur Wunderland in Hamburg, the largest model city in the world. Sure, this doesn't sound appealing to everyone, but all four of us had lots of fun. Along the way, we also visited several Hanseatic cities and the island of Rügen.

**The coolest experience**: a visit to Miniatur Wunderland

**The weirdest experience**: eating hamburgers in Hamburg (maybe it's not weird, but it's fun, so I couldn't resist - W.)

{% capture alt%} {% t alt.germany%} {% endcapture %}
{% include post-carousel.html images="assets/images/prev/45.jpg, assets/images/prev/46.jpg, assets/images/prev/47.jpg, assets/images/prev/48.jpg, assets/images/prev/49.jpg, assets/images/prev/50.jpg" alt=alt ratio="2 / 3" %}

### Ireland
In September, we wanted to go to any country where we had not been yet. The cheap flight search engine has selected Ireland. Stereotype confirmed - it was already visible from the plane that the grass is greener there. We spent a wonderful week surrounded by cliffs, castle ruins, colorful sheep and very nice, although strange, Irishmen.

**The coolest experience**: admiring the landscapes of the Dingle Peninsula
**The weirdest experience**: driving a car with an elderly gentleman whose daughter had to translate his English to our English and vice versa

{% capture alt%} {% t alt.ireland%} {% endcapture%}
{% include post-carousel.html images="assets/images/prev/54.jpg, assets/images/prev/52.jpg, assets/images/prev/53.jpg, assets/images/prev/51.jpg, assets/images/prev/55.jpg, assets/images/prev/56.jpg, assets/images/prev/57.jpg, assets/images/prev/58.jpg, assets/images/prev/59.jpg, assets/images/prev/60.jpg, assets/images/prev/61.jpg, assets/images/prev/62.jpg" alt=alt ratio="2 / 3" %}

Above we wrote only about longer trips abroad. To this we could add numerous trips to various corners (mainly mountainous) of Poland and surrounding countries. It is not that we find them uninteresting - it is simply difficult to count everything and fit in one post ... We also did not include individual trips, such as Wera's trips to Rome or Maciej's lonely hikes in Ukrainian mountains.

If we ever have enough time and inspiration, we will write more on the adventures of the last five years in a more advanced form.
