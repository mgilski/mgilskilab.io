---
title: "Gdańsk - European Solidarity Center"
image: "/assets/images/ecs/9.jpg"
categories:
- categories.poland
- categories.practical
featured: false
---


**Over the last decade, several truly unique historical museums have been established in Poland, ones that you visit not so much by viewing the exhibits in display cases, but by participating in a kind of story. The European Solidarity Center, which we recently visited, belongs to this type of facility.**

<!-- more -->

## What can you find in the museum?
As the name suggests, the museum is devoted to the history and achievements of NSZZ Solidarność. The location itself is symbolic, because the ECS building was erected right next to the **Gdańsk Shipyard**, where the famous strikes took place. Even tourists who don’t want to visit the museum come here to see the Monument to the Fallen Shipyard Workers, the so-called **Gdańsk Crosses**.
The European Solidarity Center is impressive even from the outside, thanks to its original architectural solutions, but the most interesting things are, of course, inside. There are **seven rooms**, arranged in a way as to take visitors from the genesis of Solidarity to the far-reaching effects of its activities. Such a description would probably fit most history museums… So what makes the ECS different?

{% include post-carousel-folder.html folder="ecs" images="9" alt="Europejskie centrum solidarności" ratio="3 / 4" %}

## What’s so cool about it?
Let's start with the hard evidence that the European Solidarity Center made a great impression on us. We spent **over 4 hours** there! All this time we were guided by our audioguides. The immersive comments that came from our headphones are the first important asset. The second was undoubtedly the variety of forms. Interactive maps, films, large-scale exhibits (e.g. Anna Walentynowicz’s crane), entire reconstructed rooms and various infographics are just some examples. We had so many stimuli that it was hard to get bored. If you like museums that can be described with such epithets as "modern" or "multimedia", the ECS will definitely not disappoint you.

{% include post-carousel-folder.html folder="ecs" images="1" alt="Europejskie centrum solidarności" ratio="4 / 3" %}

## Sightseeing route
On the ground floor of the museum, tickets are bought and audioguides are collected. Although the building is large and the room system is quite intricate, it's hard to get lost as long as you follow the directions from the headphones. The system is location-aware, so the narrator simply continues the story as you move through the rooms. Importantly, the recordings can be stopped or replayed at any time.

### Room A - The Birth of Solidarity
In the first part of the tour you will learn the most important information about the Gdańsk Shipyard and the 1980 strike. You do not need to know anything about this episode in the history of Poland, because the events are presented in a very accessible form. The most important exhibit in room A are the original boards on which the strikers wrote their demands.

{% include post-carousel-folder.html folder="ecs" images="3" alt="Europejskie centrum solidarności" ratio="3 / 4" %}

### Room B - Strength of the Powerless
The next room presents the repression of Poles by the communist authorities, including the brutal suppression of the 1970 protests. You can also learn a bit about the role of the church at this time. I don't think anyone will be surprised that a papamobile is standing there ...

### Room C - Solidarity and hope
At this stage of the tour, we are accompanied by the already registered NSZZ Solidarność, and thus, the tone of the arrangement becomes much more positive. We liked that the exhibitions also showed the popular culture of that time.

{% include post-carousel-folder.html folder="ecs" images="5,6" alt="Europejskie centrum solidarności" ratio="3 / 4" %}

### Room D - War with society
The leitmotif here is martial law. The audio guide presents the most important facts, you can see Jaruzelski's speech, in every corner there are interesting facts from everyday life at this specific time. We learn, for example, that on the day of the introduction of martial law there was no Teleranek program, which was beloved by children [for me this is not new, because my mother remembers it and told me about it - W.]

### Room E - The road to democracy
This part of the museum is devoted to the Round Table talks and the first partially free elections. We especially remember the wall lined with election posters of Solidarity candidates to the Sejm.

### Room F - The Triumph of Freedom and Room G - Culture of Peaceful Transformation
The last two rooms summarize the tour, with a cursory presentation of the fall of communism in other countries of Central and Eastern Europe.

{% include post-carousel-folder.html folder="ecs" images="7" alt="Europejskie centrum solidarności" ratio="3 / 4" %}

### Observation deck
After such a long sightseeing tour, probably not everyone wants to go to the observation deck, but it’s definitely worth it. The panorama of the shipyard and the city seen from the roof looks really impressive, and thanks to the audioguide you know what you’re looking at at the moment.

{% include post-carousel-folder.html folder="ecs" images="8" alt="Europejskie centrum solidarności" ratio="3 / 4" %}ecs
9## Practical information
- admission ticket costs **PLN 25** (discount PLN 20, PLN 70 for family up to 5 people), the price includes the rental of an audioguide
- it took us over 4 hours to explore the whole thing, but it can certainly be done faster, as long as you just listen politely to the voice on the handset and do not stop at the information boards, etc.
- the ECS can be reached by tram or bus, there is also a large parking lot (normally it is paid, but the fee can be deducted from the ticket price to the museum)
