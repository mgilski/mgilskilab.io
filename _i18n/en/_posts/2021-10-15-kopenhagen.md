---
title: What to see in Kopenhagen? One day in Denmark's capital"
image: "/assets/images/kopenhagen/3.jpg"
feature_text: "Opis naszych poprzednich podróży"
categories:
- categories.denmark
- categories.articles
featured: false
---

**There’s a mermaid in Warsaw, canals in Amsterdam, and in general, Stockholm has always seemed more interesting to me than the other Scandinavian capitals. In that case, is it worth visiting Copenhagen? If so, does it make sense to stop for only one day? Here’s my very subjective opinion on that.**

<!-- more -->

## Overall impressions
One day may seem insufficient to visit Copenhagen, but I had no choice because that’s the time the organizers of the **study tour** I participated in planned for seeing the city. In retrospect, I think it would probably be cooler to stay longer to see a few more museums and the Rosenborg Castle. Nevertheless, one day was enough for a walk along the trail of all the iconic places in Copenhagen. A guided tour, combined with an evening wandering around a totally non-touristic part of the city, left me with the conviction that the capital of Denmark would certainly be a good place to live in, but I would not call it a tourist must-see.

## What is worth seeing in Copenhagen in one day?
Copenhagen has a population of eight hundred thousand, so it can hardly be called a small city. Luckily for people who prefer to visit on foot (which I definitely belong to), all the most interesting, or at least the most famous places and monuments are within walking distance from the center.

### Nyhavn
When I was wondering what to see in Copenhagen, the main thing that came to my mind was an image of narrow, colorful houses perched by the canal. I was a bit disappointed because only one street looked like that - Nyhavn. I am not surprised that the city has made a calling card out of this place. There is a unique history associated with it, because although today it’s primarily a mainstay of horrendously expensive pubs, some 70 years ago Nyhavn was famous mainly for typical port taverns and brothels.

{% include post-carousel-folder.html folder="kopenhagen" images="2" alt="Monuments in Kopenhagen" ratio="4 / 3" %}

### The mermaid and surroundings
You could say that being in Copenhagen and not seeing the Little Mermaid statue is like being in Paris and missing the Eiffel Tower. But not really. The tower can be seen from everywhere, but you have to go to the Mermaid to see it. Does it make any sense? Probably yes, but only because of the symbolism. The sculpture itself is not stunning. It is small, permanently surrounded by tourists, and in the background you can see a garbage incinerator.

{% include post-carousel-folder.html folder="kopenhagen" images="6" alt="Monuments in Kopenhagen" ratio="4 / 3" %}

### Amalienborg
The Danish Royal Residence is another must-see. It’s interesting because it consists of four identical palaces. The guards in traditional uniforms and those funny fur hats (Bermycas), that the guards at Buckingham also wear, add to the atmosphere. There is also a Marble Church next to Amalienborg, which is hard to miss due to its huge green dome.

{% include post-carousel-folder.html folder="kopenhagen" images="5" alt="Monuments in Kopenhagen" ratio="4 / 3" %}

### Canal cruise
Visiting Copenhagen can be spiced up with a cruise along the canals that cut across the historic center. During the 45-minute trip I took part in, we saw Nyhavn and the Little Mermaid from the boat's perspective. There was a guide on board, telling us about the places we passed in Danish and English, but the sound system was terrible, so I couldn’t hear a lot of the information.

{% include post-carousel-folder.html folder="kopenhagen" images="4" alt="Monuments in Kopenhagen" ratio="4 / 3" %}

### Christiania
The Free City of Christiania is a part of Copenhagen that was granted a certain degree of independence in the 1970s. For years, state law didn’t fully apply here, in particular, ablind eye was turned on drug trafficking. Does a "decent tourist" have to visit Christiania? In my opinion, it doesn’t hurt to take a short walk, if only because of street art. However, if you are expecting a real hippie mecca, you will probably be put off by stalls with T-shirts and gadgets. In broad daylight, I felt completely safe there. It was hard to miss the occasional person in a state of obvious intoxication or the smell of marijuana floating in the air, but this is part of the legend of this place.

{% include post-carousel-folder.html folder="kopenhagen" images="1" alt="Monuments in Kopenhagen" ratio="4 / 3" %}

## An evening discovery
Finally, a short anecdote, which I will undoubtedly remember the most from my stay in Copenhagen. On the first evening, my friend and I went for a walk around the **Nørrebro** district, where we stayed for the night. In order not to wander completely aimlessly, we chose the closest large fragment of green on the map as our goal. Once we entered the park gate, we were first surprised by the age-old tombstones appearing here and there, then by the signs with arrows and the name **Hans Christian Andersen**. Yes, we came across the most famous cemetery in Denmark by accident, and just before closing, we found the author's grave with tons of beautiful fairy tales.

## Practical information
- in Copenhagen, my group stayed at the **A&O** hotel. If someone doesn’t know, it’s a German chain of approx. 40 hotels and hostels all over Europe. So far, I’ve used their services several times. The standard is similar everywhere, i.e. without luxury, but everything is clean and nicely decorated. Very good price / quality ratio
- this time I ate in the hotel, but I used to pay attention to **prices in pubs**. The difference between the center, and even our district of Nørrebro, seemed to me astronomical. Don't listen to people who say that everything costs a fortune in Copenhagen. If you move away from the tourists, it’s possible to find kebabs/falafels for 30-35 DKK
- the easiest way to travel around the city is **by public transport** (4 metro lines and numerous buses) or **by bike**. If you don’t have your own with you, just create an account on bycyklen.dk and rent one. It costs DKK 12 for every 20 minutes
